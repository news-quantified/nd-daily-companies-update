﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class NdUtilityHsLogCompany
    {
        public int AccountId { get; set; }
        public int CompanyHsid { get; set; }
        public string CompanyDomain { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public int Id { get; set; }
        public string InputJson { get; set; }
        public string AddedContacts { get; set; }
        public bool? FoundInHs { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public string ActionType { get; set; }
    }
}
