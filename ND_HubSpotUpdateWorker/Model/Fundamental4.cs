﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class Fundamental4
    {
        public string Exchange { get; set; }
        public string Name { get; set; }
        public string CUSIP { get; set; }
        public double? SICCode { get; set; }
        public string NAICSCode { get; set; }
        public double? ClassificationsMap { get; set; }
        public string Symbol { get; set; }
        public int? BlockTradeShares { get; set; }
        public string CIK { get; set; }
        public string URL { get; set; }
        public int IndustryCategory { get; set; }
        public string Industry { get; set; }
        public string Sector { get; set; }
        public string BusinessAddress { get; set; }
        public string MailingAddress { get; set; }
        public string InvestorRelationsURL { get; set; }
        public bool? HasOptions { get; set; }
    }
}
