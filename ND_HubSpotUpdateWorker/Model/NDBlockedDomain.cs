﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class NDBlockedDomain
    {
        public string Domain { get; set; }
    }
}
