﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class ND_UTility_HS_LogCompany
    {
        public int AccountId { get; set; }
        public int CompanyHSId { get; set; }
        public string CompanyDomain { get; set; }
        public bool isSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public int Id { get; set; }
        public string InputJSON { get; set; }
        public string AddedContacts { get; set; }
        public bool? FoundInHS { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public string ActionType { get; set; }
    }
}
