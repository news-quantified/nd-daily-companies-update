﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    // Scaffold-DbContext <<ConnectionString "NQDBEntities">> Microsoft.EntityFrameworkCore.SqlServer -OutputDir Model -Tables ClearbitCompanyAPI,ClearbitPersonAPI,Fundamental,Fundamental4,ND_UTility_HS_LogCompanies,NDBlockedDomains,NDcrmMaster,NDNews,ParserNewswires -f -UseDatabaseNames

    public partial class NQDBContext : DbContext
    {
        private static IConfigurationRoot configuration { get; set; }

        public Action<string> Log { get; set; }

        public NQDBContext()
        {
        }

        public NQDBContext(DbContextOptions<NQDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClearbitCompanyAPI> ClearbitCompanyAPIs { get; set; }
        public virtual DbSet<ClearbitPersonAPI> ClearbitPersonAPIs { get; set; }
        public virtual DbSet<Fundamental> Fundamentals { get; set; }
        public virtual DbSet<Fundamental4> Fundamental4 { get; set; }
        public virtual DbSet<NDBlockedDomain> NDBlockedDomains { get; set; }
        public virtual DbSet<NDNews> NDNews { get; set; }
        public virtual DbSet<ND_UTility_HS_LogCompany> ND_UTility_HS_LogCompanies { get; set; }
        public virtual DbSet<NDcrmMaster> NDcrmMasters { get; set; }
        public virtual DbSet<ParserNewswire> ParserNewswires { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                configuration ??= new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();

                optionsBuilder.UseSqlServer(configuration.GetConnectionString("NQDBEntities"),
                    options =>
                    {
                        options.CommandTimeout(1800);
                        options.EnableRetryOnFailure(10);
                    });

                optionsBuilder.LogTo(s => Log?.Invoke(s));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<ClearbitCompanyAPI>(entity =>
            {
                entity.HasKey(e => e.domain);

                entity.ToTable("ClearbitCompanyAPI");

                entity.Property(e => e.domain)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FromService).IsUnicode(false);

                entity.Property(e => e.LastUpdateFromClearbit).HasColumnType("datetime");

                entity.Property(e => e.Last_Manual_Verification_Date).HasColumnType("datetime");

                entity.Property(e => e.Marketplace_tag).IsUnicode(false);

                entity.Property(e => e.Tags).IsUnicode(false);

                entity.Property(e => e.address)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.alexa_globalRank).HasColumnName("alexa.globalRank");

                entity.Property(e => e.alexa_usRank).HasColumnName("alexa.usRank");

                entity.Property(e => e.angellist_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.avatar");

                entity.Property(e => e.angellist_bio)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.bio");

                entity.Property(e => e.angellist_blog)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.blog");

                entity.Property(e => e.angellist_followers).HasColumnName("angellist.followers");

                entity.Property(e => e.angellist_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.handle");

                entity.Property(e => e.angellist_site)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.site");

                entity.Property(e => e.categories).IsUnicode(false);

                entity.Property(e => e.category_industry)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("category.industry");

                entity.Property(e => e.category_industryGroup)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("category.industryGroup");

                entity.Property(e => e.category_naicsCode)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("category.naicsCode");

                entity.Property(e => e.category_sector)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("category.sector");

                entity.Property(e => e.category_sicCode)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("category.sicCode");

                entity.Property(e => e.category_subIndustry)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("category.subIndustry");

                entity.Property(e => e.contacts)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.crunchbase_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("crunchbase.handle");

                entity.Property(e => e.description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.domainAliases).IsUnicode(false);

                entity.Property(e => e.email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.facebook_followers).HasColumnName("facebook.followers");

                entity.Property(e => e.facebook_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("facebook.handle");

                entity.Property(e => e.facebook_likes).HasColumnName("facebook.likes");

                entity.Property(e => e.facebook_talking_about_count).HasColumnName("facebook.talking_about_count");

                entity.Property(e => e.founders).IsUnicode(false);

                entity.Property(e => e.geo_city)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.city");

                entity.Property(e => e.geo_country)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.country");

                entity.Property(e => e.geo_countryCode)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.countryCode");

                entity.Property(e => e.geo_lat).HasColumnName("geo.lat");

                entity.Property(e => e.geo_lng).HasColumnName("geo.lng");

                entity.Property(e => e.geo_postalCode)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.postalCode");

                entity.Property(e => e.geo_state)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.state");

                entity.Property(e => e.geo_stateCode)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.stateCode");

                entity.Property(e => e.geo_streetName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.streetName");

                entity.Property(e => e.geo_streetNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.streetNumber");

                entity.Property(e => e.geo_subPremise)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.subPremise");

                entity.Property(e => e.google_rank).HasColumnName("google.rank");

                entity.Property(e => e.googleplus_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("googleplus.handle");

                entity.Property(e => e.id)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.identifiers_usEIN)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("identifiers.usEIN");

                entity.Property(e => e.keywords).IsUnicode(false);

                entity.Property(e => e.legalName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.linkedin_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("linkedin.handle");

                entity.Property(e => e.location)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.logo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.metrics_annualRevenue).HasColumnName("metrics.annualRevenue");

                entity.Property(e => e.metrics_employeesRange)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("metrics.employeesRange");

                entity.Property(e => e.metrics_estimatedAnnualRevenue)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("metrics.estimatedAnnualRevenue");

                entity.Property(e => e.metrics_fiscalYearEnd).HasColumnName("metrics.fiscalYearEnd");

                entity.Property(e => e.metrics_marketCap).HasColumnName("metrics.marketCap");

                entity.Property(e => e.name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.parent_domain)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("parent.domain");

                entity.Property(e => e.phone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.site_title)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("site.title");

                entity.Property(e => e.site_url)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("site.url");

                entity.Property(e => e.skype_handle)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("skype.handle");

                entity.Property(e => e.stocktwits)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.stocktwits_rank).HasColumnName("stocktwits.rank");

                entity.Property(e => e.tech).IsUnicode(false);

                entity.Property(e => e.techCategories).IsUnicode(false);

                entity.Property(e => e.timeZone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.twitter_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.avatar");

                entity.Property(e => e.twitter_bio)
                    .IsUnicode(false)
                    .HasColumnName("twitter.bio");

                entity.Property(e => e.twitter_favorites).HasColumnName("twitter.favorites");

                entity.Property(e => e.twitter_followers).HasColumnName("twitter.followers");

                entity.Property(e => e.twitter_following).HasColumnName("twitter.following");

                entity.Property(e => e.twitter_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.handle");

                entity.Property(e => e.twitter_id)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.id");

                entity.Property(e => e.twitter_location)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.location");

                entity.Property(e => e.twitter_site)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.site");

                entity.Property(e => e.twitter_statuses).HasColumnName("twitter.statuses");

                entity.Property(e => e.type)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ultimate_parent_domain)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("ultimate_parent.domain");

                entity.Property(e => e.youtube)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClearbitPersonAPI>(entity =>
            {
                entity.HasKey(e => e.email);

                entity.ToTable("ClearbitPersonAPI");

                entity.Property(e => e.email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdateFromClearbit).HasColumnType("datetime");

                entity.Property(e => e.LindedinUrlCompany).IsUnicode(false);

                entity.Property(e => e.LindedinUrlPerson).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.aboutme_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("aboutme.avatar");

                entity.Property(e => e.aboutme_bio)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("aboutme.bio");

                entity.Property(e => e.aboutme_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("aboutme.handle");

                entity.Property(e => e.angellist_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.avatar");

                entity.Property(e => e.angellist_bio)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.bio");

                entity.Property(e => e.angellist_blog)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.blog");

                entity.Property(e => e.angellist_followers).HasColumnName("angellist.followers");

                entity.Property(e => e.angellist_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.handle");

                entity.Property(e => e.angellist_site)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("angellist.site");

                entity.Property(e => e.avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.bio)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.employment_domain)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("employment.domain");

                entity.Property(e => e.employment_name)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("employment.name");

                entity.Property(e => e.employment_role)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("employment.role");

                entity.Property(e => e.employment_seniority)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("employment.seniority");

                entity.Property(e => e.employment_subRole)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("employment.subRole");

                entity.Property(e => e.employment_title)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("employment.title");

                entity.Property(e => e.facebook_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("facebook.handle");

                entity.Property(e => e.gender)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.geo_city)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.city");

                entity.Property(e => e.geo_country)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.country");

                entity.Property(e => e.geo_countryCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("geo.countryCode");

                entity.Property(e => e.geo_lat)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.lat");

                entity.Property(e => e.geo_lng)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.lng");

                entity.Property(e => e.geo_postalCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("geo.postalCode");

                entity.Property(e => e.geo_state)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("geo.state");

                entity.Property(e => e.geo_stateCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("geo.stateCode");

                entity.Property(e => e.geo_streetName)
                    .IsUnicode(false)
                    .HasColumnName("geo.streetName");

                entity.Property(e => e.geo_streetNumber)
                    .IsUnicode(false)
                    .HasColumnName("geo.streetNumber");

                entity.Property(e => e.geo_subPremise)
                    .IsUnicode(false)
                    .HasColumnName("geo.subPremise");

                entity.Property(e => e.github_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("github.avatar");

                entity.Property(e => e.github_blog)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("github.blog");

                entity.Property(e => e.github_company)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("github.company");

                entity.Property(e => e.github_followers)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("github.followers");

                entity.Property(e => e.github_following)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("github.following");

                entity.Property(e => e.github_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("github.handle");

                entity.Property(e => e.github_id).HasColumnName("github.id");

                entity.Property(e => e.googleplus_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("googleplus.handle");

                entity.Property(e => e.gravatar_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("gravatar.avatar");

                entity.Property(e => e.gravatar_avatars)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("gravatar.avatars");

                entity.Property(e => e.gravatar_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("gravatar.handle");

                entity.Property(e => e.gravatar_urls)
                    .IsUnicode(false)
                    .HasColumnName("gravatar.urls");

                entity.Property(e => e.id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.linkedin_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("linkedin.handle");

                entity.Property(e => e.location)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.name_familyName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("name.familyName");

                entity.Property(e => e.name_fullName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("name.fullName");

                entity.Property(e => e.name_givenName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("name.givenName");

                entity.Property(e => e.site)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.skype_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("skype.handle");

                entity.Property(e => e.timeZone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.twitter_avatar)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.avatar");

                entity.Property(e => e.twitter_bio)
                    .IsUnicode(false)
                    .HasColumnName("twitter.bio");

                entity.Property(e => e.twitter_favorites).HasColumnName("twitter.favorites");

                entity.Property(e => e.twitter_followers).HasColumnName("twitter.followers");

                entity.Property(e => e.twitter_following).HasColumnName("twitter.following");

                entity.Property(e => e.twitter_handle)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.handle");

                entity.Property(e => e.twitter_id)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.id");

                entity.Property(e => e.twitter_location)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.location");

                entity.Property(e => e.twitter_site)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("twitter.site");

                entity.Property(e => e.twitter_statuses).HasColumnName("twitter.statuses");
            });

            modelBuilder.Entity<Fundamental>(entity =>
            {
                entity.HasKey(e => e.FundamentalKey);

                entity.ToTable("Fundamental");

                entity.HasIndex(e => new { e.JDate, e.Symbol }, "NCI_Fundamental_JDate_Symbol")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Symbol, "NCI_Fundamental_Symbol")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.JDate, "NCI_JDate")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.FundamentalKey).HasMaxLength(255);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Exchange).HasMaxLength(255);

                entity.Property(e => e.JDate).HasMaxLength(255);

                entity.Property(e => e.Symbol).HasMaxLength(25);
            });

            modelBuilder.Entity<Fundamental4>(entity =>
            {
                entity.HasKey(e => e.Symbol);

                entity.ToTable("Fundamental4");

                entity.HasIndex(e => e.Name, "NCI_Fundamental4_Name")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.SICCode, "NCI_Fundamental4_SICCode")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => new { e.Symbol, e.Name }, "NCI_Fundamental4_SymbolName")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => new { e.Symbol, e.SICCode }, "NCI_Fundamental4_SymbolSICCode")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.IndustryCategory, "NCI_IndustryCategory")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Symbol).HasMaxLength(25);

                entity.Property(e => e.BusinessAddress).IsUnicode(false);

                entity.Property(e => e.CIK)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.CUSIP).HasMaxLength(255);

                entity.Property(e => e.Exchange).HasMaxLength(255);

                entity.Property(e => e.Industry)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvestorRelationsURL).IsUnicode(false);

                entity.Property(e => e.MailingAddress).IsUnicode(false);

                entity.Property(e => e.NAICSCode).HasMaxLength(10);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Sector)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.URL).IsUnicode(false);
            });

            modelBuilder.Entity<NDBlockedDomain>(entity =>
            {
                entity.HasKey(e => e.Domain)
                    .HasName("PK_BlockedDomains");

                entity.Property(e => e.Domain)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NDNews>(entity =>
            {
                entity.HasKey(e => e.NewsKey)
                    .HasName("PK__NDNews__4946A7C0D3699DC9");

                entity.Property(e => e.NewsKey).IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactInfo).IsUnicode(false);

                entity.Property(e => e.DBAddTime).HasColumnType("datetime");

                entity.Property(e => e.DateLine)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Domains)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Headline)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ID)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.NewsWireURL).IsUnicode(false);

                entity.Property(e => e.PhoneNumbers)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PubTime).HasColumnType("datetime");

                entity.Property(e => e.Source)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Tags).IsUnicode(false);
            });

            modelBuilder.Entity<ND_UTility_HS_LogCompany>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK_ND_UTility_HS_LogCompanies_Id")
                    .IsClustered(false);

                entity.Property(e => e.ActionType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AddedContacts).IsUnicode(false);

                entity.Property(e => e.CompanyDomain)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateTimeUpdated).HasColumnType("datetime");

                entity.Property(e => e.ErrorMessage).IsUnicode(false);

                entity.Property(e => e.InputJSON).IsUnicode(false);
            });

            modelBuilder.Entity<NDcrmMaster>(entity =>
            {
                entity.HasKey(e => e.CompanyUrl);

                entity.ToTable("NDcrmMaster");

                entity.Property(e => e.CompanyUrl).IsUnicode(false);

                entity.Property(e => e.AdditionalDomains).IsUnicode(false);

                entity.Property(e => e.AgencyUrl).IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstRelease).HasColumnType("datetime");

                entity.Property(e => e.LastRelease).HasColumnType("datetime");

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.ND_HubSpot).HasColumnType("datetime");

                entity.Property(e => e.ND_HubSpotPrioritized).HasColumnType("datetime");

                entity.Property(e => e.PIDs).IsUnicode(false);

                entity.Property(e => e.PublicCompanyDashboardURL)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ticker)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.prospect_level)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParserNewswire>(entity =>
            {
                entity.Property(e => e.Agency)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.Dateline)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.Error).HasMaxLength(250);

                entity.Property(e => e.LastUpdated).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.NewsID)
                    .HasMaxLength(900)
                    .IsUnicode(false);

                entity.Property(e => e.NewsReleaseDate).HasColumnType("datetime");

                entity.Property(e => e.Phone).HasMaxLength(250);

                entity.Property(e => e.RelateLinks)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Source).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.Property(e => e.Type).HasMaxLength(500);

                entity.Property(e => e.UncnounPart).HasMaxLength(500);

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }

   
}
