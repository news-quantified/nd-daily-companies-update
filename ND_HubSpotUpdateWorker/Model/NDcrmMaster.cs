﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class NDcrmMaster
    {
        public string CompanyName { get; set; }
        public string CompanyUrl { get; set; }
        public string CompanyType { get; set; }
        public string AgencyUrl { get; set; }
        public int? TotalReleases { get; set; }
        public DateTime? FirstRelease { get; set; }
        public DateTime? LastRelease { get; set; }
        public int? PrevYearReleases { get; set; }
        public int? CurrYearReleases { get; set; }
        public double? EAVHighCurrYear { get; set; }
        public double? EAVHighPrevYear { get; set; }
        public bool? Clearbit { get; set; }
        public DateTime? ND_HubSpot { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool? AmazonLinks { get; set; }
        public bool? Earnings { get; set; }
        public string PIDs { get; set; }
        public bool? Multimedia_Sender { get; set; }
        public double? StoryCount { get; set; }
        public string AdditionalDomains { get; set; }
        public double? EAVLowCurrYear { get; set; }
        public double? EAVLowPrevYear { get; set; }
        public bool? StaticMedia_Sender { get; set; }
        public int? StaticMediaCount_Curr { get; set; }
        public int? StaticMediaCount_Prev { get; set; }
        public int? MultiMediaCount_Curr { get; set; }
        public int? MultiMediaCount_Prev { get; set; }
        public double? ND_ECVCurrYear { get; set; }
        public double? ND_ECVPrevYear { get; set; }
        public long? hubspot_owner_id { get; set; }
        public string prospect_level { get; set; }
        public string Ticker { get; set; }
        public string PublicCompanyDashboardURL { get; set; }
        public DateTime? ND_HubSpotPrioritized { get; set; }
    }
}
