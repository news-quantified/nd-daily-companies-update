﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class Fundamental
    {
        public string Exchange { get; set; }
        public string Symbol { get; set; }
        public double? PERatio { get; set; }
        public int? MktCap { get; set; }
        public DateTime? Date { get; set; }
        public string JDate { get; set; }
        public string FundamentalKey { get; set; }
        public double? AvgVol { get; set; }
        public double? Volatility { get; set; }
        public double? Yield { get; set; }
    }
}
