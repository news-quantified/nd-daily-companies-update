﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class ClearbitCompanyAPI
    {
        public string domain { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string legalName { get; set; }
        public string site_url { get; set; }
        public string site_title { get; set; }
        public string categories { get; set; }
        public string description { get; set; }
        public string founders { get; set; }
        public int? raised { get; set; }
        public string location { get; set; }
        public int? google_rank { get; set; }
        public int? alexa_usRank { get; set; }
        public int? alexa_globalRank { get; set; }
        public string facebook_handle { get; set; }
        public string linkedin_handle { get; set; }
        public string twitter_handle { get; set; }
        public string twitter_id { get; set; }
        public int? twitter_followers { get; set; }
        public int? twitter_following { get; set; }
        public string twitter_location { get; set; }
        public string twitter_site { get; set; }
        public string twitter_avatar { get; set; }
        public string angellist_handle { get; set; }
        public string angellist_bio { get; set; }
        public string angellist_blog { get; set; }
        public string angellist_site { get; set; }
        public int? angellist_followers { get; set; }
        public string angellist_avatar { get; set; }
        public string crunchbase_handle { get; set; }
        public int? employees { get; set; }
        public string logo { get; set; }
        public bool? emailProvider { get; set; }
        public bool? personal { get; set; }
        public string Marketplace_tag { get; set; }
        public DateTime? Last_Manual_Verification_Date { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string googleplus_handle { get; set; }
        public string youtube { get; set; }
        public string stocktwits { get; set; }
        public string contacts { get; set; }
        public string address { get; set; }
        public int? facebook_likes { get; set; }
        public int? facebook_talking_about_count { get; set; }
        public int? stocktwits_rank { get; set; }
        public string keywords { get; set; }
        public int? facebook_followers { get; set; }
        public string skype_handle { get; set; }
        public bool isHide { get; set; }
        public string FromService { get; set; }
        public string Tags { get; set; }
        public string domainAliases { get; set; }
        public string category_sector { get; set; }
        public string category_industryGroup { get; set; }
        public string category_industry { get; set; }
        public string category_subIndustry { get; set; }
        public string category_sicCode { get; set; }
        public string category_naicsCode { get; set; }
        public int? foundedYear { get; set; }
        public string timeZone { get; set; }
        public int? utcOffset { get; set; }
        public string geo_streetNumber { get; set; }
        public string geo_streetName { get; set; }
        public string geo_subPremise { get; set; }
        public string geo_city { get; set; }
        public string geo_state { get; set; }
        public string geo_stateCode { get; set; }
        public string geo_postalCode { get; set; }
        public string geo_country { get; set; }
        public string geo_countryCode { get; set; }
        public double? geo_lat { get; set; }
        public double? geo_lng { get; set; }
        public string identifiers_usEIN { get; set; }
        public string metrics_employeesRange { get; set; }
        public int? metrics_marketCap { get; set; }
        public int? metrics_annualRevenue { get; set; }
        public string metrics_estimatedAnnualRevenue { get; set; }
        public int? metrics_fiscalYearEnd { get; set; }
        public string twitter_bio { get; set; }
        public string type { get; set; }
        public string tech { get; set; }
        public string parent_domain { get; set; }
        public string ultimate_parent_domain { get; set; }
        public DateTime? LastUpdateFromClearbit { get; set; }
        public int? twitter_statuses { get; set; }
        public int? twitter_favorites { get; set; }
        public string techCategories { get; set; }
    }
}
