﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class ClearbitPersonAPI
    {
        public string email { get; set; }
        public string id { get; set; }
        public string name_givenName { get; set; }
        public string name_familyName { get; set; }
        public string name_fullName { get; set; }
        public string gender { get; set; }
        public string location { get; set; }
        public string geo_city { get; set; }
        public string geo_state { get; set; }
        public string geo_country { get; set; }
        public string geo_lat { get; set; }
        public string geo_lng { get; set; }
        public string bio { get; set; }
        public string site { get; set; }
        public string avatar { get; set; }
        public string employment_name { get; set; }
        public string employment_title { get; set; }
        public string employment_domain { get; set; }
        public string facebook_handle { get; set; }
        public string github_handle { get; set; }
        public string github_avatar { get; set; }
        public string github_company { get; set; }
        public string github_blog { get; set; }
        public string github_followers { get; set; }
        public string github_following { get; set; }
        public string twitter_handle { get; set; }
        public string twitter_id { get; set; }
        public int? twitter_followers { get; set; }
        public int? twitter_following { get; set; }
        public string twitter_location { get; set; }
        public string twitter_site { get; set; }
        public string twitter_avatar { get; set; }
        public string linkedin_handle { get; set; }
        public string googleplus_handle { get; set; }
        public string angellist_handle { get; set; }
        public string angellist_bio { get; set; }
        public string angellist_blog { get; set; }
        public string angellist_site { get; set; }
        public int? angellist_followers { get; set; }
        public string angellist_avatar { get; set; }
        public string aboutme_handle { get; set; }
        public string aboutme_bio { get; set; }
        public string aboutme_avatar { get; set; }
        public string gravatar_handle { get; set; }
        public string gravatar_urls { get; set; }
        public string gravatar_avatar { get; set; }
        public string gravatar_avatars { get; set; }
        public bool? fuzzy { get; set; }
        public string skype_handle { get; set; }
        public string LindedinUrlPerson { get; set; }
        public string LindedinUrlCompany { get; set; }
        public string Phone { get; set; }
        public string timeZone { get; set; }
        public int? utcOffset { get; set; }
        public string employment_role { get; set; }
        public string employment_subRole { get; set; }
        public string employment_seniority { get; set; }
        public int? github_id { get; set; }
        public int? twitter_statuses { get; set; }
        public int? twitter_favorites { get; set; }
        public bool? emailProvider { get; set; }
        public DateTime? LastUpdateFromClearbit { get; set; }
        public string geo_countryCode { get; set; }
        public string geo_postalCode { get; set; }
        public string geo_stateCode { get; set; }
        public string geo_streetName { get; set; }
        public string geo_streetNumber { get; set; }
        public string geo_subPremise { get; set; }
        public string twitter_bio { get; set; }
    }
}
