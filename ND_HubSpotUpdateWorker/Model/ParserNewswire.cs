﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class ParserNewswire
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string NewsID { get; set; }
        public DateTime? NewsReleaseDate { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public string UncnounPart { get; set; }
        public string Error { get; set; }
        public string CompanyName { get; set; }
        public string Title { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string RelateLinks { get; set; }
        public string Agency { get; set; }
        public int ID { get; set; }
        public string Dateline { get; set; }
        public int? CheckByDebounce { get; set; }
    }
}
