﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ND_HubSpotUpdateWorker.Model
{
    public partial class NDNews
    {
        public string NewsKey { get; set; }
        public string ID { get; set; }
        public string Company { get; set; }
        public string Headline { get; set; }
        public DateTime? PubTime { get; set; }
        public string ContactInfo { get; set; }
        public int? StoryCount { get; set; }
        public string NewsWireURL { get; set; }
        public string DateLine { get; set; }
        public bool? EngLang { get; set; }
        public int? StaticMediaCount { get; set; }
        public int? MultiMediaCount { get; set; }
        public double? ReleaseValueLow { get; set; }
        public double? ReleaseValueHigh { get; set; }
        public string Source { get; set; }
        public string PhoneNumbers { get; set; }
        public string Domains { get; set; }
        public bool? IsEarnings { get; set; }
        public string Tags { get; set; }
        public long? HubSpotID { get; set; }
        public DateTime? DBAddTime { get; set; }
        public bool? RemovedHubspot { get; set; }
        public double? ND_ECV { get; set; }
    }
}
