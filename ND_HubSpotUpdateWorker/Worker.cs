using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ND_HubSpotUpdateWorker.Core;
using ND_HubSpotUpdateWorker.Dto;
using ND_HubSpotUpdateWorker.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker
{
    public class Worker : BackgroundService
    {
        private readonly IHostApplicationLifetime _hostApplicationLifetime;
        private readonly ILogger<Worker> _logger;

        public Worker(IHostApplicationLifetime hostApplicationLifetime, ILogger<Worker> logger)
        {
            _hostApplicationLifetime = hostApplicationLifetime;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    try
                    {
                        //_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                        //Globals.MaxDegreeOfParallelism = 1; // test

                        List<CompanyProcessContainer> companyList = null;

                        //var companyL = await DBFunc.GetCompanyListAsync("mavennext.com");
                        //var hsContacts = HubSpotHelper.GetContactsByEmails(new List<string>() { "customersuccess@mavennext.com" });
                        //var emailR = DBFunc.GetAllEmailRecordsFromDomains(new List<string> { "mavennext.com" });
                        //var newContacts = DBFunc.GetAllContactsFromDomains(emailR);

                        // update hi-priority data
                        if (Globals.Stage1Param || Globals.NoStageParams)
                        {
                            companyList = await DBFunc.GetCompanyListAsync(Globals.SingleDomainParam);
                            Stage1Updater.Run(companyList);
                        }


                        // create new companies
                        if (Globals.Stage2Param || Globals.NoStageParams)
                        {
                            companyList ??= await DBFunc.GetCompanyListAsync(Globals.SingleDomainParam);
                            Stage2Updater.Run(companyList);
                        }


                        // add contacts, update low-priority data
                        if (Globals.Stage3Param || Globals.NoStageParams)
                        {
                            companyList ??= await DBFunc.GetCompanyListAsync(Globals.SingleDomainParam);
                            Stage3Updater.Run(companyList);
                        }

                        //var updatedHi = companyList.Where(x => x.UpdatedHi).Count();
                        //var created = companyList.Where(x => x.Created).Count();
                        //var updatedLow = companyList.Where(x => x.UpdatedLow).Count();
                        //var contactsAdded = companyList.SelectMany(x => x.AddedContactEmails).Count();
                        //var contactsNotAdded = companyList.SelectMany(x => x.NotAddedContactEmails).Count();

#if (!DEBUG)
                        MailHelper.SendReport();
#endif
                    }
                    catch (Exception e)
                    {
                        ConsoleHelper.WriteLine(ConsoleColor.Red, $"Main - exception: {e.Message}");
                    }

                    _logger.LogInformation("Worker finish at: {time}", DateTimeOffset.Now);
                    break;
                }
            }
            finally
            {
                _hostApplicationLifetime.StopApplication();
            }
        }
    }
}
