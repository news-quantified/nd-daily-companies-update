﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Helper
{
    public static class JsonHelper
    {
        public static bool IsValidJson(string strInput, string domain)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"Json validation failed: {jex.Message}. Domain: {domain}");
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"Json validation failed: {ex.ToString()}. Domain: {domain}");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
