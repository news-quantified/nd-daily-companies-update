﻿using ND_HubSpotUpdateWorker.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Helper
{
    #region v1-v2

    public class HS_Contacts
    {
        public long ContactId { get; set; }
        public string email { get; set; }
    }

    [DataContract]
    public class HS_ContactsByEmail
    {
        [JsonExtensionData]
        public Dictionary<string, JToken> child { get; set; }

        public List<ContactSummaryItem> contacts => child
            .Select(x =>
            {
                var p = x.Value.ToObject<HS_ContactByEmail>().properties;

                return new ContactSummaryItem()
                {
                    contact_id = x.Key,
                    email = p?.email?.value,
                    bio = p?.bio?.value,
                    city = p?.city?.value,
                    company = p?.company?.value,
                    country = p?.country?.value,
                    employment_role = p?.employment_role?.value,
                    employment_subrole = p?.employment_subrole?.value,
                    firstname = p?.firstname?.value,
                    full_name = p?.full_name?.value,
                    jobtitle = p?.jobtitle?.value,
                    lastname = p?.lastname?.value,
                    linkedin = p?.linkedin?.value,
                    location = p?.location?.value,
                    nq_contact_date = DateTime.TryParse(p?.nq_contact_date?.value, out var dt) ? dt : null,
                    nq_source = p?.nq_source?.value,
                    phone = p?.phone?.value,
                    seniority = p?.seniority?.value,
                    state = p?.state?.value
                };
            })
            .ToList();
    }

    [DataContract]
    public class HS_ContactByEmail
    {
        [DataMember(Name = "vid")]
        public long vid { get; set; }
        [DataMember(Name = "properties")]
        public HS_ContactProperties properties { get; set; }
    }

    [DataContract]
    public class HS_ContactProperties
    {
        [DataMember(Name = "firstname")]
        public HS_value firstname { get; set; }

        [DataMember(Name = "lastname")]
        public HS_value lastname { get; set; }

        [DataMember(Name = "email")]
        public HS_value email { get; set; }

        [DataMember(Name = "company")]
        public HS_value company { get; set; }

        [DataMember(Name = "jobtitle")]
        public HS_value jobtitle { get; set; }

        [DataMember(Name = "phone")]
        public HS_value phone { get; set; }

        [DataMember(Name = "nq_source")]
        public HS_value nq_source { get; set; }

        [DataMember(Name = "nq_contact_date")]
        public HS_value nq_contact_date { get; set; }

        [DataMember(Name = "full_name")]
        public HS_value full_name { get; set; }

        [DataMember(Name = "location")]
        public HS_value location { get; set; }

        [DataMember(Name = "city")]
        public HS_value city { get; set; }

        [DataMember(Name = "state")]
        public HS_value state { get; set; }

        [DataMember(Name = "country")]
        public HS_value country { get; set; }

        [DataMember(Name = "linkedin")]
        public HS_value linkedin { get; set; }

        [DataMember(Name = "employment_role")]
        public HS_value employment_role { get; set; }

        [DataMember(Name = "employment_subrole")]
        public HS_value employment_subrole { get; set; }

        [DataMember(Name = "seniority")]
        public HS_value seniority { get; set; }

        [DataMember(Name = "bio")]
        public HS_value bio { get; set; }
    }

    [DataContract]
    public class HS_value
    {
        [DataMember(Name = "value")]
        public string value { get; set; }

    }

    [DataContract]
    public class HS_Contact
    {
        [DataMember(Name = "hasMore")]
        public bool hasMore { get; set; }
        [DataMember(Name = "vidOffset")]
        public long vidOffset { get; set; }
        [DataMember(Name = "contacts")]
        public contacts[] contacts { get; set; }
    }

    [DataContract]
    public class contacts
    {
        [DataMember(Name = "vid")]
        public long vid { get; set; }
        [DataMember(Name = "isContact")]
        public bool isContact { get; set; }
        [DataMember(Name = "identities")]
        public identities[] identities { get; set; }
    }

    [DataContract]
    public class identities
    {
        [DataMember(Name = "identity")]
        public identity[] identity { get; set; }
    }

    [DataContract]
    public class identity
    {
        [DataMember(Name = "value")]
        public string value { get; set; }
        [DataMember(Name = "type")]
        public string type { get; set; }
    }

    [DataContract]
    public class HS_CompanyList
    {
        [DataMember(Name = "has-more")]
        public bool hasMore { get; set; }
        [DataMember(Name = "offset")]
        public long offset { get; set; }
        [DataMember(Name = "companies")]
        public results[] companies { get; set; }
    }

    [DataContract]
    public class HS_Company
    {
        [DataMember(Name = "results")]
        public results[] results { get; set; }
    }

    [DataContract]
    public class results
    {
        [DataMember(Name = "companyId")]
        public long companyId { get; set; }
        [DataMember(Name = "properties")]
        public properties properties { get; set; }
    }

    [DataContract]
    public class properties
    {
        [DataMember(Name = "twitterfollowers")]
        public HS_value twitterfollowers { get; set; }
        [DataMember(Name = "domain")]
        public HS_value domain { get; set; }
        [DataMember(Name = "annualrevenue")]
        public HS_value annualrevenue { get; set; }
        [DataMember(Name = "city")]
        public HS_value city { get; set; }
        [DataMember(Name = "company_email")]
        public HS_value company_email { get; set; }
        [DataMember(Name = "country")]
        public HS_value country { get; set; }
        [DataMember(Name = "description")]
        public HS_value description { get; set; }
        [DataMember(Name = "industry")]
        public HS_value industry { get; set; }
        [DataMember(Name = "name")]
        public HS_value name { get; set; }
        [DataMember(Name = "numberofemployees")]
        public HS_value numberofemployees { get; set; }
        [DataMember(Name = "phone")]
        public HS_value phone { get; set; }
        [DataMember(Name = "public_company_exchange")]
        public HS_value public_company_exchange { get; set; }
        [DataMember(Name = "related_agency")]
        public HS_value related_agency { get; set; }
        [DataMember(Name = "state")]
        public HS_value state { get; set; }
        [DataMember(Name = "address")]
        public HS_value address { get; set; }
        [DataMember(Name = "company_type")]
        public HS_value company_type { get; set; }
        [DataMember(Name = "address2")]
        public HS_value address2 { get; set; }
        [DataMember(Name = "ticker_symbol")]
        public HS_value ticker_symbol { get; set; }
        [DataMember(Name = "timezone")]
        public HS_value timezone { get; set; }
        [DataMember(Name = "total_money_raised")]
        public HS_value total_money_raised { get; set; }
        [DataMember(Name = "website")]
        public HS_value website { get; set; }
        [DataMember(Name = "working_with_competitor")]
        public HS_value working_with_competitor { get; set; }
        [DataMember(Name = "market_cap")]
        public HS_value market_cap { get; set; }
        [DataMember(Name = "founded_year")]
        public HS_value founded_year { get; set; }
        [DataMember(Name = "facebook_company_page")]
        public HS_value facebook_company_page { get; set; }
        [DataMember(Name = "facebookfans")]
        public HS_value facebookfans { get; set; }
        [DataMember(Name = "googleplus_page")]
        public HS_value googleplus_page { get; set; }
        [DataMember(Name = "linkedin_company_page")]
        public HS_value linkedin_company_page { get; set; }
        [DataMember(Name = "twitterbio")]
        public HS_value twitterbio { get; set; }
        [DataMember(Name = "twitterhandle")]
        public HS_value twitterhandle { get; set; }
        [DataMember(Name = "investor_relations_site")]
        public HS_value investor_relations_site { get; set; }
        [DataMember(Name = "newsroom_url")]
        public HS_value newsroom_url { get; set; }
        [DataMember(Name = "current_year_billing")]
        public HS_value current_year_billing { get; set; }
        [DataMember(Name = "hubspot_owner_id")]
        public HS_value hubspot_owner_id { get; set; }
        [DataMember(Name = "distributes_earnings")]
        public HS_value distributes_earnings { get; set; }
        [DataMember(Name = "release_count_current_year_")]
        public HS_value release_count_current_year_ { get; set; }
        [DataMember(Name = "release_count_prior_year_")]
        public HS_value release_count_prior_year_ { get; set; }
        [DataMember(Name = "eav_high_current_year_")]
        public HS_value eav_high_current_year_ { get; set; }
        [DataMember(Name = "eav_high_prior_year_")]
        public HS_value eav_high_prior_year_ { get; set; }
        [DataMember(Name = "eav_low_current_year_")]
        public HS_value eav_low_current_year_ { get; set; }
        [DataMember(Name = "eav_low_prior_year_")]
        public HS_value eav_low_prior_year_ { get; set; }
        [DataMember(Name = "word_count_average_")]
        public HS_value word_count_average_ { get; set; }
        [DataMember(Name = "multimedia_sender")]
        public HS_value multimedia_sender { get; set; }
        [DataMember(Name = "static_media_sender")]
        public HS_value static_media_sender { get; set; }

        [DataMember(Name = "amazon_affiliate_links")]
        public HS_value amazon_affiliate_links { get; set; }

        [DataMember(Name = "static_media_count_current_year_")]
        public HS_value static_media_count_current_year_ { get; set; }

        [DataMember(Name = "static_media_count_previous_year_")]
        public HS_value static_media_count_previous_year_ { get; set; }

        [DataMember(Name = "multimedia_count_current_year_")]
        public HS_value multimedia_count_current_year_ { get; set; }

        [DataMember(Name = "multimedia_count_previous_year_")]
        public HS_value multimedia_count_previous_year_ { get; set; }

        [DataMember(Name = "ecv_prior_year_")]
        public HS_value ecv_prior_year_ { get; set; }

        [DataMember(Name = "ecv_current_year_")]
        public HS_value ecv_current_year_ { get; set; }

        [DataMember(Name = "prospect_level")]
        public HS_value prospect_level { get; set; }

        [DataMember(Name = "competitor")]
        public HS_value competitor { get; set; }

        [DataMember(Name = "word_count")]
        public HS_value word_count { get; set; }

        [DataMember(Name = "public_company_dashboard_nq_")]
        public HS_value public_company_dashboard_nq_ { get; set; }

        [DataMember(Name = "is_public")]
        public HS_value is_public { get; set; }


    }


    [DataContract]
    public class prop_value
    {
        [DataMember(Name = "value")]
        public string value { get; set; }
    }

    #endregion v1-v2

    #region v3

    [DataContract]
    public class HS_ListV3<T>
    {
        [DataMember(Name = "results")]
        public HS_ResultsV3<T>[] results { get; set; }
        [DataMember(Name = "paging")]
        public HS_pagingV3 paging { get; set; }
    }

    [DataContract]
    public class HS_pagingV3
    {
        [DataMember(Name = "next")]
        public HS_nextV3 next { get; set; }
    }

    [DataContract]
    public class HS_nextV3
    {
        [DataMember(Name = "after")]
        public string after { get; set; }
        [DataMember(Name = "link")]
        public string link { get; set; }
    }

    [DataContract]
    public class HS_ResultsV3<T>
    {
        [DataMember(Name = "id")]
        public long id { get; set; }
        [DataMember(Name = "properties")]
        public T properties { get; set; }

        [DataMember(Name = "createdAt")]
        public string createdAt { get; set; }
        [DataMember(Name = "updatedAt")]
        public string updatedAt { get; set; }
        [DataMember(Name = "archived")]
        public bool archived { get; set; }
    }

    #endregion v3

}
