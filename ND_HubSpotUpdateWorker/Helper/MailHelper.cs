﻿using ND_HubSpotUpdateWorker.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Helper
{
    public class MailHelper
    {
        public const string SYSemail = "system@newsquantified.com";
        public const string SYSpassword = "lpwd1986";

        public static void SendEmails(string action, string reason, string mes)
        {
            foreach (var email in Globals.EmailsForReport)
            {
                SendEmail(action, reason, mes, email);
            }
        }

        public static void SendEmail(string action, string reason, string mes, string emailTo)
        {
            System.Net.Mail.MailAddress from = new(SYSemail);
            System.Net.Mail.MailAddress to = new(emailTo);

            MailMessage m = new(from, to);

            var loginInfo = new NetworkCredential(SYSemail, SYSpassword);
            var smtpClient = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = loginInfo
            };

            m.Subject = string.Format("ND_HubSpotUpdateWorker: {0}", action);
            m.Body = string.Format("ND_HubSpotUpdateWorker: {0}<br />Reason: {1}<br />Exception Message: {2}<br />", action, reason, mes);
            m.IsBodyHtml = true;

            smtpClient.Send(m);
        }

        public static void SendReport()
        {
            foreach (var email in Globals.EmailsForReport)
            {
                SendReport(email);
            }
        }

        public static void SendReport(string emailTo)
        {
            MailAddress from = new(SYSemail);
            MailAddress to = new(emailTo);

            MailMessage m = new(from, to);

            var loginInfo = new NetworkCredential(SYSemail, SYSpassword);
            var smtpClient = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = loginInfo
            };

            m.Subject = string.Format("ND_DailyUpdate Report {0}", DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
            m.Body = string.Format(@"ND_DailyUpdate Report <b>{0}</b>:<br /><br /> 
Added new companies - <b>{1}</b><br />
New companies not added due to error - <b>{2}</b><br />
HI - Updated Companies - <b>{3}</b><br />
HI - Companies not updated due to error - <b>{4}</b><br />
LOW - Updated Companies - <b>{5}</b><br />
LOW - Companies not updated due to error - <b>{6}</b><br />
Updated contacts - <b>{7}</b><br />
Contacts not updated due to error - <b>{8}</b><br />
Added new contacts - <b>{9}</b><br />
New contacts not added due to error - <b>{10}</b><br />", DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)
                                                       , Globals.COUNT_COMPANY_ADD
                                                       , Globals.COUNT_COMPANY_ADD_ERROR
                                                       , Globals.COUNT_COMPANY_UPDATE_HI
                                                       , Globals.COUNT_COMPANY_UPDATE_ERROR_HI
                                                       , Globals.COUNT_COMPANY_UPDATE_LOW
                                                       , Globals.COUNT_COMPANY_UPDATE_ERROR_LOW
                                                       , Globals.COUNT_CONTACT_UPDATE
                                                       , Globals.COUNT_CONTACT_UPDATE_ERROR
                                                       , Globals.COUNT_CONTACT_ADD
                                                       , Globals.COUNT_CONTACT_ADD_ERROR);
            m.IsBodyHtml = true;

            smtpClient.Send(m);
        }

        public static void SendEmail(string content, string emailTo)
        {
            System.Net.Mail.MailAddress from = new(SYSemail);
            System.Net.Mail.MailAddress to = new(emailTo);

            MailMessage m = new(from, to);

            var loginInfo = new NetworkCredential(SYSemail, SYSpassword);
            var smtpClient = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = loginInfo
            };

            m.Subject = string.Format("ND_HubSpotUpdateWorker Message");
            m.Body = string.Format("Message:<br />{0}", content);
            m.IsBodyHtml = true;

            smtpClient.Send(m);
        }
    }
}
