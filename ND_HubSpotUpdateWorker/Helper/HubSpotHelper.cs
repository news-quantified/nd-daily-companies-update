﻿using ND_HubSpotUpdateWorker.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using ND_HubSpotUpdateWorker.Core;
using System.Globalization;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using ND_HubSpotUpdateWorker.Extensions;
using Newtonsoft.Json.Linq;
using RateLimiter;
using ComposableAsync;
using ND_HubSpotUpdateWorker.Attributes;

namespace ND_HubSpotUpdateWorker.Helper
{
    public static class HubSpotHelper
    {
        public static DateTime DailyLimitExceededDate = DateTime.MinValue; // must be synchronized with timezone of HS account
        public static volatile int ExceededErrors = 0;
        public static volatile int MaxExceededErrors = 300;

        public static volatile int LIMIT_DailyRemaining = 500000;
        public static volatile int LIMIT_Interval10sRemaining = 150;
        public static volatile int LIMIT_SecondlyRemaining = 15;

        private static readonly TimeLimiter requestLimiter = TimeLimiter.GetFromMaxCountByInterval(13, TimeSpan.FromSeconds(1));

        const string HAPI_KEY = "ba3c33fe-d409-42c5-9965-deebdb7313d1"; /*ND*/

        private static readonly string listOfIncorrectSymbolsForDomain = "@; )?";


        public static CompanySummaryItem AddCompanyToHubSpot(CompanySummaryItem company, int logId)
        {
            CompanySummaryItem CompanyItem = null;

            try
            {
                var uri = string.Format("https://api.hubapi.com/companies/v2/companies?hapikey=" + HAPI_KEY);
                var jsonResult = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Post, uri, company.json, attempts: 1).GetAwaiter().GetResult();

                if (!string.IsNullOrEmpty(jsonResult))
                {
                    DataContractJsonSerializer json = new(typeof(HS_Company));
                    var obj = (HS_Company)json.ReadObject(new MemoryStream(Encoding.Unicode.GetBytes(jsonResult)));
                    if (obj != null && obj.results != null && obj.results.Length > 0)
                    {
                        foreach (var res in obj.results)
                        {
                            try
                            {
                                CompanyItem = MapHSResultToCompanySummaryItem(res);
                            }
                            catch (Exception ex)
                            {
                                ConsoleHelper.WriteLine(ConsoleColor.Red, $"Map created company - exception: {ex.Message}");
                            }
                        }
                    }
                }
                DBFunc.CompanyAddedToHS(logId, "Create", company.json, jsonResult);
            }
            catch (Exception ex)
            {
                DBFunc.CompanyNotAddedToHS(logId, "Create", company.json, ex.Message);
                throw;
            }

            return CompanyItem;
        }

        public static void RecheckCompanyBeforeUpdate(CompanySummaryItem company)
        {
            if (company == null || string.IsNullOrEmpty(company.domain))
            {
                throw new Exception("Company null or domain is empty");
            }

            if (!string.IsNullOrEmpty(company.public_company_exchange))
            {
                if (company.public_company_exchange == "NASDAQ-NMS")
                    company.public_company_exchange = "NASDAQ";

                if (company.public_company_exchange == "NASDAQ-OTC")
                    company.public_company_exchange = "OTC Markets";

                if (company.public_company_exchange == "TSE")
                    company.public_company_exchange = "Toronto Stock Exchange";

                if (company.public_company_exchange != "TSE" && company.public_company_exchange != "NASDAQ-OTC" && company.public_company_exchange != "NASDAQ-NMS")
                    company.public_company_exchange = string.Empty;
            }

            if (!string.IsNullOrEmpty(company.competitor))
            {
                try
                {
                    List<string> t1 = company.competitor.Split(new string[] { "," }, StringSplitOptions.None).ToList();
                    List<string> t2 = new();

                    if (t1 != null && t1.Count > 0)
                    {
                        foreach (string t in t1)
                        {
                            if (t == "BW" && !t2.Contains("Business Wire")) t2.Add("Business Wire");
                            if (t == "PR" && !t2.Contains("PR Newswire")) t2.Add("PR Newswire");
                            if (t == "GN" && !t2.Contains("Globe Newswire")) t2.Add("Globe Newswire");
                            if (t == "MW" && !t2.Contains("Marketwired")) t2.Add("Marketwired");
                            if (t == "PW" && !t2.Contains("PR Web")) t2.Add("PR Web");
                            if (t == "CN" && !t2.Contains("Canada Newswire")) t2.Add("Canada Newswire");
                        }
                    }

                    if (t2 != null && t2.Count > 0)
                    {
                        company.competitor = string.Join(";", t2.ToArray());
                    }
                    else
                    {
                        company.competitor = string.Empty;
                    }

                }
                catch { }
            }
        }

        public static List<string> UpdateCompanyToHubSpot(CompanyProcessContainer companyProcess, bool fullModel)
        {
            List<string> result = new();

            foreach (var hsCompany in companyProcess.HSRecords.Where(x => !string.IsNullOrEmpty(x.json)))
            {
                var uri = string.Format("https://api.hubapi.com/companies/v2/companies/{0}?hapikey={1}", hsCompany.companyId, HAPI_KEY);
                var res = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Put, uri, hsCompany.json).GetAwaiter().GetResult();
                result.Add(res);
            }

            return result;
        }


        public static string CompanyToJson(CompanySummaryItem company, bool fullModel)
        {
            var props = company.GetType().GetProperties()
                .Where(p => !Attribute.IsDefined(p, typeof(NotUpdateAttribute)) &&
                            (fullModel || Attribute.IsDefined(p, typeof(Update_HiAttribute)) ));

            var propsStr = props.Select(p => "{" + p.Name + "}").Join(string.Empty);

            foreach (var prop in props)
            {
                SetJsonParam(ref propsStr,
                    paramName: prop.Name,
                    paramValue: prop.GetValue(company, null),
                    historicalValue: company.HistoricalClone == null ? null : prop.GetValue(company.HistoricalClone, null),
                    updateIfNotEmpty: Attribute.IsDefined(prop, typeof(Update_IfNotEmptyAttribute)));
            }

            if (string.IsNullOrEmpty(propsStr))
                return null;

            var json = @"{ {objectId}
                           ""properties"": [" + propsStr + "]" + " }";

            if (company.companyId != 0)
            {
                // for batch update
                json = json.Replace("{objectId}", $@" ""objectId"": {company.companyId}, ");
            }
            else
            {
                // to create company
                json = json.Replace("{objectId}", "");
            }

            return json;
        }

        private static void SetJsonParam(ref string json, string paramName, object paramValue, object historicalValue, bool updateIfNotEmpty)
        {
            string jsonValue = null;

            if (paramValue?.ToString() != historicalValue?.ToString())
            {
                switch (paramValue)
                {
                    case string s when !updateIfNotEmpty || !string.IsNullOrEmpty(s):
                        jsonValue = JsonConvert.ToString(s);
                        break;
                    case long v when !updateIfNotEmpty || v != 0:
                        jsonValue = "\"" + v.ToString(CultureInfo.InvariantCulture) + "\"";
                        break;
                    case double d when !updateIfNotEmpty || d != 0:
                        jsonValue = "\"" + d.ToString(CultureInfo.InvariantCulture) + "\"";
                        break;
                    case bool b when !updateIfNotEmpty || !b:
                        jsonValue = "\"" + b.ToString() + "\"";
                        break;
                }
            }

            if ((jsonValue != null) &&
                (!updateIfNotEmpty || !string.IsNullOrEmpty(jsonValue)))
            {
                json = json.Replace("{" + paramName + "}", $@", {{ ""name"": ""{paramName}"",
                                                                   ""value"": {jsonValue}
                                                                }}");
            }

            json = json.Replace("{" + paramName + "}", string.Empty).Trim();

            if (json.StartsWith(","))
            {
                json = json.Remove(0, 1);
            }
        }

        public static void UpdateGroupOfCompaniesToHubSpot(IEnumerable<CompanyProcessContainer> companies)
        {
            var jsons = companies
                .SelectMany(x => x.HSRecords)
                .Where(x => !string.IsNullOrEmpty(x.json))
                .Select(x => x.json);

            if (!jsons.Any())
                return;

            var json = "[" + jsons.Join() + "]";
            var uri = string.Format("https://api.hubapi.com/companies/v1/batch-async/update?hapikey={0}", HAPI_KEY);
            var jsonResult = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Post, uri, json).GetAwaiter().GetResult();
        }

        public static void AddContactToHubSpot(ContactSummaryItem contact)
        {
            if (contact == null || string.IsNullOrEmpty(contact.email))
            {
                throw new Exception("Contact null or email is empty");
            }

            string json = AddContact_BuildRequestBodyV1(contact);
            var uri = string.Format("https://api.hubapi.com/contacts/v1/contact/?hapikey=" + HAPI_KEY);
            _ = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Post, uri, json, attempts: 1).GetAwaiter().GetResult();
        }

        public static void AddContactsToHubSpot(IEnumerable<ContactSummaryItem> contacts)
        {
            string json = AddOrUpdateContacts_BuildRequestBodyV3(contacts, isUpdate: false);
            var uri = "https://api.hubapi.com/crm/v3/objects/contacts/batch/create?hapikey=" + HAPI_KEY;
            _ = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Post, uri, json).GetAwaiter().GetResult();
        }

        public static void UpdateBatchOfContacts(IEnumerable<ContactSummaryItem> contacts)
        {
            string json = AddOrUpdateContacts_BuildRequestBodyV3(contacts, isUpdate: true);
            var uri = "https://api.hubapi.com/crm/v3/objects/contacts/batch/update?hapikey=" + HAPI_KEY;
            _ = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Post, uri, json).GetAwaiter().GetResult();
        }

        private static string AddContact_BuildRequestBodyV1(ContactSummaryItem contact)
        {
            /*FOR ND ONLY*/
            var json = @"
            {
                ""properties"": [
                {
                    ""property"": ""email"",
                    ""value"": ""{0}""
                },
                {
                    ""property"": ""lifecyclestage"",
                    ""value"": ""lead""
                }{1}{2}{3}{4}{5}{6}{7}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}
                ]
            }
            ";

            json = json.Replace("{0}", contact.email.ToLower());



            /*1*/
            if (!string.IsNullOrEmpty(contact.company))
            {
                json = json.Replace("{1}", string.Format(@",
                                                        {{
                                                            ""property"": ""company"",
                                                            ""value"": ""{0}""
                                                        }}", contact.company));

            }
            else
            {
                json = json.Replace("{1}", string.Empty);
            }
            /*2*/
            if (!string.IsNullOrEmpty(contact.firstname))
            {
                json = json.Replace("{2}", string.Format(@",
                                                            {{
                                                                ""property"": ""firstname"",
                                                                ""value"": ""{0}""
                                                            }}", contact.firstname));
            }
            else
            {
                json = json.Replace("{2}", string.Empty);
            }


            /*3*/
            if (!string.IsNullOrEmpty(contact.jobtitle))
            {
                json = json.Replace("{3}", string.Format(@",
                                                            {{
                                                                ""property"": ""jobtitle"",
                                                                ""value"": ""{0}""
                                                            }}", contact.jobtitle));
            }
            else
            {
                json = json.Replace("{3}", string.Empty);
            }

            /*4*/
            if (!string.IsNullOrEmpty(contact.lastname))
            {
                json = json.Replace("{4}", string.Format(@",
                                                            {{
                                                                ""property"": ""lastname"",
                                                                ""value"": ""{0}""
                                                            }}", contact.lastname));
            }
            else
            {
                json = json.Replace("{4}", string.Empty);
            }
            /*5*/
            if (contact.nq_contact_date.HasValue)
            {
                json = json.Replace("{5}", string.Format(@",
                                                            {{
                                                                ""property"": ""nq_contact_date"",
                                                                ""value"": ""{0}""
                                                            }}", contact.nq_contact_date.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)));
            }
            else
            {
                json = json.Replace("{5}", string.Empty);
            }
            /*6*/
            if (!string.IsNullOrEmpty(contact.nq_source))
            {
                json = json.Replace("{6}", string.Format(@",
                                                            {{
                                                                ""property"": ""nq_source"",
                                                                ""value"": ""{0}""
                                                            }}", contact.nq_source));
            }
            else
            {
                json = json.Replace("{6}", string.Empty);
            }
            /*7*/
            if (!string.IsNullOrEmpty(contact.phone))
            {
                json = json.Replace("{7}", string.Format(@",
                                                            {{
                                                                ""property"": ""phone"",
                                                                ""value"": ""{0}""
                                                            }}", contact.phone));
            }
            else
            {
                json = json.Replace("{7}", string.Empty);
            }
            /*9*/
            if (!string.IsNullOrEmpty(contact.full_name))
            {
                json = json.Replace("{9}", string.Format(@",
                                                            {{
                                                                ""property"": ""full_name"",
                                                                ""value"": ""{0}""
                                                            }}", contact.full_name));
            }
            else
            {
                json = json.Replace("{9}", string.Empty);
            }
            /*10*/
            if (!string.IsNullOrEmpty(contact.location))
            {
                json = json.Replace("{10}", string.Format(@",
                                                            {{
                                                                ""property"": ""location"",
                                                                ""value"": ""{0}""
                                                            }}", contact.location));
            }
            else
            {
                json = json.Replace("{10}", string.Empty);
            }
            /*11*/
            if (!string.IsNullOrEmpty(contact.city))
            {
                json = json.Replace("{11}", string.Format(@",
                                                            {{
                                                                ""property"": ""city"",
                                                                ""value"": ""{0}""
                                                            }}", contact.city));
            }
            else
            {
                json = json.Replace("{11}", string.Empty);
            }
            /*12*/
            if (!string.IsNullOrEmpty(contact.state))
            {
                json = json.Replace("{12}", string.Format(@",
                                                            {{
                                                                ""property"": ""state"",
                                                                ""value"": ""{0}""
                                                            }}", contact.state));
            }
            else
            {
                json = json.Replace("{12}", string.Empty);
            }
            /*13*/
            if (!string.IsNullOrEmpty(contact.country))
            {
                json = json.Replace("{13}", string.Format(@",
                                                            {{
                                                                ""property"": ""country"",
                                                                ""value"": ""{0}""
                                                            }}", contact.country));
            }
            else
            {
                json = json.Replace("{13}", string.Empty);
            }
            /*14*/
            if (!string.IsNullOrEmpty(contact.linkedin))
            {
                json = json.Replace("{14}", string.Format(@",
                                                            {{
                                                                ""property"": ""linkedin"",
                                                                ""value"": ""{0}""
                                                            }}", contact.linkedin));
            }
            else
            {
                json = json.Replace("{14}", string.Empty);
            }
            /*15*/
            if (!string.IsNullOrEmpty(contact.employment_role))
            {
                json = json.Replace("{15}", string.Format(@",
                                                            {{
                                                                ""property"": ""employment_role"",
                                                                ""value"": ""{0}""
                                                            }}", contact.employment_role));
            }
            else
            {
                json = json.Replace("{15}", string.Empty);
            }
            /*16*/
            if (!string.IsNullOrEmpty(contact.employment_subrole))
            {
                json = json.Replace("{16}", string.Format(@",
                                                            {{
                                                                ""property"": ""employment_subrole"",
                                                                ""value"": ""{0}""
                                                            }}", contact.employment_subrole));
            }
            else
            {
                json = json.Replace("{16}", string.Empty);
            }
            /*17*/
            if (!string.IsNullOrEmpty(contact.seniority))
            {
                json = json.Replace("{17}", string.Format(@",
                                                            {{
                                                                ""property"": ""seniority"",
                                                                ""value"": ""{0}""
                                                            }}", contact.seniority));
            }
            else
            {
                json = json.Replace("{17}", string.Empty);
            }
            /*18*/
            if (!string.IsNullOrEmpty(contact.bio))
            {
                json = json.Replace("{18}", string.Format(@",
                                                            {{
                                                                ""property"": ""bio"",
                                                                ""value"": ""{0}""
                                                            }}", contact.bio));
            }
            else
            {
                json = json.Replace("{18}", string.Empty);
            }

            return json;
        }

        private static string AddOrUpdateContacts_BuildRequestBodyV3(IEnumerable<ContactSummaryItem> contacts, bool isUpdate)
        {
            var listJsonProps = new List<string>();

            foreach (var contact in contacts)
            {

            /*FOR ND ONLY*/
                var json = @"
                {
                    {contactIdString}
                    ""properties"": {
                        ""email"": ""{0}""
                       ,""lifecyclestage"": ""lead""
                       {1}{2}{3}{4}{5}{6}{7}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}
                       }
                }
                ";

                if (isUpdate)
                {
                    json = json.Replace("{contactIdString}", @$" ""id"": ""{contact.contact_id}"", ");
                }
                else
                {
                    json = json.Replace("{contactIdString}", string.Empty);
                }
                

                json = json.Replace("{0}", contact.email.ToLower());



                /*1*/
                if (!string.IsNullOrEmpty(contact.company))
                {
                    json = json.Replace("{1}", string.Format(@" ,""company"": ""{0}"" ", contact.company));

                }
                else
                {
                    json = json.Replace("{1}", string.Empty);
                }
                /*2*/
                if (!string.IsNullOrEmpty(contact.firstname))
                {
                    json = json.Replace("{2}", string.Format(@", ""firstname"": ""{0}"" ", contact.firstname));
                }
                else
                {
                    json = json.Replace("{2}", string.Empty);
                }


                /*3*/
                if (!string.IsNullOrEmpty(contact.jobtitle))
                {
                    json = json.Replace("{3}", string.Format(@", ""jobtitle"": ""{0}"" ", contact.jobtitle));
                }
                else
                {
                    json = json.Replace("{3}", string.Empty);
                }

                /*4*/
                if (!string.IsNullOrEmpty(contact.lastname))
                {
                    json = json.Replace("{4}", string.Format(@", ""lastname"": ""{0}"" ", contact.lastname));
                }
                else
                {
                    json = json.Replace("{4}", string.Empty);
                }
                /*5*/
                if (contact.nq_contact_date.HasValue)
                {
                    json = json.Replace("{5}", string.Format(@", ""nq_contact_date"": ""{0}""
                                                            ", contact.nq_contact_date.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)));
                }
                else
                {
                    json = json.Replace("{5}", string.Empty);
                }
                /*6*/
                if (!string.IsNullOrEmpty(contact.nq_source))
                {
                    json = json.Replace("{6}", string.Format(@",
                                                                ""nq_source"": ""{0}""
                                                            ", contact.nq_source));
                }
                else
                {
                    json = json.Replace("{6}", string.Empty);
                }
                /*7*/
                if (!string.IsNullOrEmpty(contact.phone))
                {
                    json = json.Replace("{7}", string.Format(@",
                                                                ""phone"": ""{0}""
                                                            ", contact.phone));
                }
                else
                {
                    json = json.Replace("{7}", string.Empty);
                }
                /*9*/
                if (!string.IsNullOrEmpty(contact.full_name))
                {
                    json = json.Replace("{9}", string.Format(@",
                                                                ""full_name"": ""{0}""
                                                            ", contact.full_name));
                }
                else
                {
                    json = json.Replace("{9}", string.Empty);
                }
                /*10*/
                if (!string.IsNullOrEmpty(contact.location))
                {
                    json = json.Replace("{10}", string.Format(@",
                                                                ""location"": ""{0}""
                                                            ", contact.location));
                }
                else
                {
                    json = json.Replace("{10}", string.Empty);
                }
                /*11*/
                if (!string.IsNullOrEmpty(contact.city))
                {
                    json = json.Replace("{11}", string.Format(@",
                                                                ""city"": ""{0}""
                                                            ", contact.city));
                }
                else
                {
                    json = json.Replace("{11}", string.Empty);
                }
                /*12*/
                if (!string.IsNullOrEmpty(contact.state))
                {
                    json = json.Replace("{12}", string.Format(@",
                                                                ""state"": ""{0}""
                                                            ", contact.state));
                }
                else
                {
                    json = json.Replace("{12}", string.Empty);
                }
                /*13*/
                if (!string.IsNullOrEmpty(contact.country))
                {
                    json = json.Replace("{13}", string.Format(@",
                                                                ""country"": ""{0}""
                                                            ", contact.country));
                }
                else
                {
                    json = json.Replace("{13}", string.Empty);
                }
                /*14*/
                if (!string.IsNullOrEmpty(contact.linkedin))
                {
                    json = json.Replace("{14}", string.Format(@",
                                                                ""linkedin"": ""{0}""
                                                            ", contact.linkedin));
                }
                else
                {
                    json = json.Replace("{14}", string.Empty);
                }
                /*15*/
                if (!string.IsNullOrEmpty(contact.employment_role))
                {
                    json = json.Replace("{15}", string.Format(@",
                                                                ""employment_role"": ""{0}""
                                                            ", contact.employment_role));
                }
                else
                {
                    json = json.Replace("{15}", string.Empty);
                }
                /*16*/
                if (!string.IsNullOrEmpty(contact.employment_subrole))
                {
                    json = json.Replace("{16}", string.Format(@",
                                                                ""employment_subrole"": ""{0}""
                                                            ", contact.employment_subrole));
                }
                else
                {
                    json = json.Replace("{16}", string.Empty);
                }
                /*17*/
                if (!string.IsNullOrEmpty(contact.seniority))
                {
                    json = json.Replace("{17}", string.Format(@",
                                                                ""seniority"": ""{0}""
                                                            ", contact.seniority));
                }
                else
                {
                    json = json.Replace("{17}", string.Empty);
                }
                /*18*/
                if (!string.IsNullOrEmpty(contact.bio))
                {
                    json = json.Replace("{18}", string.Format(@",
                                                                ""bio"": ""{0}""
                                                            ", contact.bio));
                }
                else
                {
                    json = json.Replace("{18}", string.Empty);
                }

                listJsonProps.Add(json);
            }

            var result = @" { ""inputs"": [ " + listJsonProps.Join() + " ] }";

            return result;
        }

        public static List<CompanySummaryItem> GetCompanyFromHubSpotByDomain(string domain)
        {
            List<CompanySummaryItem> results = new();

            try
            {
                string jsonRequest = @"
                {
                  ""limit"": 100,
                  ""requestOptions"": {
                    ""properties"": [
                        ""annualrevenue"",
                        ""city"",
                        ""company_email"",
                        ""company_type"",
                        ""country"",
                        ""description"",
                        ""industry"",
                        ""name"",
                        ""numberofemployees"",
                        ""phone"",
                        ""public_company_exchange"",
                        ""related_agency"",
                        ""state"",
                        ""address"",
                        ""address2"",
                        ""ticker_symbol"",
                        ""timezone"",
                        ""total_money_raised"",
                        ""website"",
                        ""market_cap"",
                        ""is_public"",
                        ""founded_year"",
                        ""facebook_company_page"",
                        ""facebookfans"",
                        ""googleplus_page"",
                        ""linkedin_company_page"",
                        ""twitterbio"",
                        ""twitterfollowers"",
                        ""twitterhandle"",
                        ""investor_relations_site"",
                        ""newsroom_url"",
                        ""competitor"",
                        ""current_year_billing"",
                        ""word_count"",
                        ""public_company_dashboard_nq_"",
                        ""hubspot_owner_id"",
                        ""domain"",
                        ""working_with_competitor"",
                        ""distributes_earnings"",
                        ""release_count_current_year_"",
                        ""release_count_prior_year_"",
                        ""eav_high_current_year_"",
                        ""eav_high_prior_year_"",
                        ""eav_low_current_year_"",
                        ""eav_low_prior_year_"",
                        ""word_count_average_"",
                        ""multimedia_sender"",
                        ""static_media_sender"",
                        ""amazon_affiliate_links"",
                        ""static_media_count_current_year_"",
                        ""static_media_count_previous_year_"",
                        ""multimedia_count_current_year_"",
                        ""multimedia_count_previous_year_"",
                        ""ecv_prior_year_"",
                        ""ecv_current_year_"",
                        ""prospect_level""
                        ]
                    },
                  ""offset"": {
                    ""isPrimary"": true,
                    ""isDeleted"": false,
                    ""companyId"": 0
                  }
                }
                ";

                var uri = string.Format("https://api.hubapi.com/companies/v2/domains/{0}/companies?hapikey={1}", domain, HAPI_KEY);
                var jsontext = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Post, uri, jsonRequest).GetAwaiter().GetResult();

                if (!string.IsNullOrEmpty(jsontext))
                {
                    DataContractJsonSerializer json = new(typeof(HS_Company));
                    var obj = (HS_Company)json.ReadObject(new MemoryStream(Encoding.Unicode.GetBytes(jsontext)));
                    if (obj != null && obj.results != null && obj.results.Length > 0)
                    {
                        foreach (var res in obj.results)
                        {
                            CompanySummaryItem CompanyItem = null;

                            try
                            {
                                CompanyItem = MapHSResultToCompanySummaryItem(res);
                            }
                            catch (Exception ex)
                            {
                                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetCompanyFromHubSpotByDomain({domain}) mapping exception: {ex.Message}");
                            }

                            if (CompanyItem != null)
                            {
                                results.Add(CompanyItem);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetCompanyFromHubSpotByDomain_EX({domain}) general exception: {ex.Message}");
#if (!DEBUG)
                MailHelper.SendEmails("HubSpotHelper", "GetCompanyFromHubSpotByDomain_EX", domain + " --->>> " + ex.Message);
#endif
                throw;
            }

            return results;
        }

        private static CompanySummaryItem MapHSResultToCompanySummaryItem(results res)
        {
            var props = res.properties;
            CompanySummaryItem companyItem = new()
            {
                companyId               = res.companyId,
                annualrevenue           = props?.annualrevenue?.value,
                city                    = props?.city?.value,
                company_email           = props?.company_email?.value,
                company_type            = props?.company_type?.value,
                country                 = props?.country?.value,
                description             = props?.description?.value,
                industry                = props?.industry?.value,
                name                    = props?.name?.value,
                numberofemployees       = props?.numberofemployees?.value,
                phone                   = props?.phone?.value,
                public_company_exchange = props?.public_company_exchange?.value,
                related_agency          = props?.related_agency?.value,
                state                   = props?.state?.value,
                address                 = props?.address?.value,
                address2                = props?.address2?.value,
                ticker_symbol           = props?.ticker_symbol?.value,
                timezone                = props?.timezone?.value,
                total_money_raised      = props?.total_money_raised?.value,
                website                 = props?.website?.value,
                market_cap              = props?.market_cap?.value,
                is_public               = bool.TryParse(props?.is_public?.value, out var valueOut) && valueOut,
                founded_year            = props?.founded_year?.value,
                facebook_company_page   = props?.facebook_company_page?.value,
                facebookfans            = props?.facebookfans?.value,
                googleplus_page         = props?.googleplus_page?.value,
                linkedin_company_page   = props?.linkedin_company_page?.value,
                twitterbio              = props?.twitterbio?.value,
                twitterfollowers        = props?.twitterfollowers?.value,
                twitterhandle           = props?.twitterhandle?.value,
                investor_relations_site = props?.investor_relations_site?.value,
                newsroom_url            = props?.newsroom_url?.value,
                competitor              = props?.competitor?.value,
                current_year_billing    = long.TryParse(props?.current_year_billing?.value, out var current_year_billing_Out) ? current_year_billing_Out : 0,
                word_count              = props?.word_count?.value,
                public_company_dashboard_nq_ = props?.public_company_dashboard_nq_?.value,
                hubspot_owner_id        = long.TryParse(props?.hubspot_owner_id?.value, out var hubspot_owner_id_Out) ? hubspot_owner_id_Out : 0,
                domain                  = props?.domain?.value,
                working_with_competitor = props?.working_with_competitor?.value,
                distributes_earnings    = props?.distributes_earnings?.value,
                release_count_current_year_ = long.TryParse(props?.release_count_current_year_?.value, out var release_count_current_year_Out) ? release_count_current_year_Out : 0,
                release_count_prior_year_ = long.TryParse(props?.release_count_prior_year_?.value, out var release_count_prior_year_Out) ? release_count_prior_year_Out : 0,
                eav_high_current_year_  = double.TryParse(props?.eav_high_current_year_?.value, NumberStyles.Any, CultureInfo.InvariantCulture, out var eav_high_current_year_Out) ? eav_high_current_year_Out : 0,
                eav_high_prior_year_    = double.TryParse(props?.eav_high_prior_year_?.value, NumberStyles.Any, CultureInfo.InvariantCulture, out var eav_high_prior_year_Out) ? eav_high_prior_year_Out : 0,
                eav_low_current_year_   = double.TryParse(props?.eav_low_current_year_?.value, NumberStyles.Any, CultureInfo.InvariantCulture, out var eav_low_current_year_Out) ? eav_low_current_year_Out : 0,
                eav_low_prior_year_     = double.TryParse(props?.eav_low_prior_year_?.value, NumberStyles.Any, CultureInfo.InvariantCulture, out var eav_low_prior_year_Out) ? eav_low_prior_year_Out : 0,
                word_count_average_     = props?.word_count_average_?.value,
                multimedia_sender       = props?.multimedia_sender?.value,
                static_media_sender     = props?.static_media_sender?.value,
                amazon_affiliate_links  = props?.amazon_affiliate_links?.value,
                static_media_count_current_year_ = long.TryParse(props?.static_media_count_current_year_?.value, out var static_media_count_current_year_Out) ? static_media_count_current_year_Out : 0,
                static_media_count_previous_year_ = long.TryParse(props?.static_media_count_previous_year_?.value, out var static_media_count_previous_year_Out) ? static_media_count_previous_year_Out : 0,
                multimedia_count_current_year_ = long.TryParse(props?.multimedia_count_current_year_?.value, out var multimedia_count_current_year_Out) ? multimedia_count_current_year_Out : 0,
                multimedia_count_previous_year_ = long.TryParse(props?.multimedia_count_previous_year_?.value, out var multimedia_count_previous_year_Out) ? multimedia_count_previous_year_Out : 0,
                ecv_prior_year_         = double.TryParse(props?.ecv_prior_year_?.value, NumberStyles.Any, CultureInfo.InvariantCulture, out var ecv_prior_year_Out) ? ecv_prior_year_Out : 0,
                ecv_current_year_       = double.TryParse(props?.ecv_current_year_?.value, NumberStyles.Any, CultureInfo.InvariantCulture, out var ecv_current_year_Out) ? ecv_current_year_Out : 0,
                prospect_level          = props?.prospect_level?.value,
            };

            return companyItem;
        }

        public static bool PublicInstancePropertiesEqual<T>(T self, T to, params string[] ignore) where T : class
        {
            if (self != null && to != null)
            {
                Type type = typeof(T);
                List<string> ignoreList = new(ignore);
                foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(to, null);

                        if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return self == to;
        }

        public static List<HS_Contacts> GetAllContactsForCompanyFromHS(long companyid)
        {
            List<HS_Contacts> list = new();

            var httpClient = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            bool _hasmore = true;
            long _offset = 0;

            do
            {
                try
                {
                    var uri = string.Format("https://api.hubapi.com/companies/v2/companies/{0}/contacts?hapikey={1}&count=100&vidOffset={2}", companyid, HAPI_KEY, _offset);
                    var jsonResult = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Get, uri, httpClient).GetAwaiter().GetResult();
                    if (!string.IsNullOrEmpty(jsonResult))
                    {
                        DataContractJsonSerializer json = new(typeof(HS_Contact));
                        var obj = (HS_Contact)json.ReadObject(new MemoryStream(Encoding.Unicode.GetBytes(jsonResult)));

                        if (obj != null)
                        {
                            _hasmore = obj.hasMore;
                            _offset = obj.vidOffset;

                            if (obj.contacts != null && obj.contacts.Length > 0)
                            {
                                foreach (var comp in obj.contacts)
                                {
                                    if (comp.isContact && comp.identities != null && comp.identities.Length > 0)
                                    {
                                        foreach (var idents in comp.identities)
                                        {
                                            if (idents.identity != null && idents.identity.Length > 0)
                                            {
                                                foreach (var id in idents.identity)
                                                {
                                                    if (!string.IsNullOrEmpty(id.type) && id.type.ToLower() == "email" && !string.IsNullOrEmpty(id.value))
                                                    {
                                                        list.Add(new HS_Contacts()
                                                        {
                                                            ContactId = comp.vid,
                                                            email = id.value,
                                                        });
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllContactsForCompanyFromHS Exception: {ex.Message}");
                }
            }
            while (_hasmore);

            return list;
        }

        public static List<ContactSummaryItem> GetContactsByEmails(List<string> emails)
        {
            List<ContactSummaryItem> result = new();
            object _lock = new();

            List<string> contactPropertyList = new()
            {
                "email",
                "firstname",
                "lastname",
                "company",
                "jobtitle",
                "phone",
                "nq_source",
                "nq_contact_date",
                "full_name",
                "city",
                "state",
                "country",
                "linkedin",
                "employment_role",
                "employment_subrole",
                "seniority",
                "bio"
            };

            Parallel.ForEach(emails.ChunkBy(100), new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, chunk =>
            {
                if (chunk.Any())
                    try
                    {
                        var emails = chunk.Select(x => "email=" + x).Join("&");
                        var properties = "property=" + contactPropertyList.Join("&property=");

                        var uri = $"https://api.hubapi.com/contacts/v1/contact/emails/batch/?{emails}&{properties}&hapikey={HAPI_KEY}";
                        var jsonResult = SendRequestAndCheckLimitsAsync(WebRequestMethods.Http.Get, uri).GetAwaiter().GetResult();
                        if (!string.IsNullOrEmpty(jsonResult))
                        {
                            var obj = JsonConvert.DeserializeObject<HS_ContactsByEmail>(jsonResult);
                            if (obj?.contacts?.Count > 0)
                            {
                                lock (_lock)
                                {
                                    result.AddRange(obj.contacts);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllContactsForCompanyFromHS Exception: {ex.Message}");
                    }
            });

            return result;
        }

        public static async Task<string> SendRequestAndCheckLimitsAsync(string method, string uri, string json = null, int attempts = 5)
        {
            return await SendRequestAndCheckLimitsAsync(method, uri, null, json, attempts);
        }

        public static async Task<string> SendRequestAndCheckLimitsAsync(string method, string uri,
            HttpClient httpClient,
            string json = null,
            int attempts = 10)
        {
            if (DailyLimitExceededDate == DateTime.Now.Date)
                throw new Exception("DailyLimit exceeded for today!");
                
            if (ExceededErrors > MaxExceededErrors)
                throw new Exception("Too many of ExceededErrors!");

            if (httpClient == null)
            {
                httpClient = new HttpClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }

            httpClient.Timeout = TimeSpan.FromMinutes(10);

            HttpContent data = null;
            if (!string.IsNullOrEmpty(json))
            {
                data = new StringContent(json, Encoding.UTF8, "application/json");
            }

            HttpResponseMessage response = null;
            string exceptionMessage;

            do
            {
                // limit MaxRequestCountByTimeInterval (12req / 10sec)
                await requestLimiter;

                exceptionMessage = null;

                try
                {
                    response = method switch
                    {
                        WebRequestMethods.Http.Post => await httpClient.PostAsync(uri, data),
                        WebRequestMethods.Http.Put => await httpClient.PutAsync(uri, data),
                        _ => await httpClient.GetAsync(uri),
                    };

                    if (response.Headers.TryGetValues("X-HubSpot-RateLimit-Daily-Remaining", out var dailyRemaining))
                    {
                        if (int.TryParse(dailyRemaining.FirstOrDefault(), out int dailyRemainingInt))
                        {
                            Interlocked.Exchange(ref LIMIT_DailyRemaining, dailyRemainingInt);

                            if (dailyRemainingInt < 10000)
                            {
                                DailyLimitExceededDate = DateTime.Now.Date;
                                throw new Exception("DailyLimit exceeded for today!");
                            }
                        }
                    }

                    if (response.Headers.TryGetValues("X-HubSpot-RateLimit-Remaining", out var interval10sRemaining))
                    {
                        if (int.TryParse(interval10sRemaining.FirstOrDefault(), out int interval10sRemainingInt))
                        {
                            Interlocked.Exchange(ref LIMIT_Interval10sRemaining, interval10sRemainingInt);

                            if (interval10sRemainingInt < 10)
                                Thread.Sleep(1000);
                        }
                    }

                    if (response.Headers.TryGetValues("X-HubSpot-RateLimit-Secondly-Remaining", out var secondlyRemaining))
                    {
                        if (int.TryParse(secondlyRemaining.FirstOrDefault(), out int secondlyRemainingInt))
                        {
                            Interlocked.Exchange(ref LIMIT_SecondlyRemaining, secondlyRemainingInt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    exceptionMessage = ex.Message;
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"HS request exception {ex.Message}. Attempts left {attempts}");
                }

                if (response?.StatusCode == (HttpStatusCode)429)
                {
                    Interlocked.Increment(ref ExceededErrors);
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"- HRLimit reached: dayly:{LIMIT_DailyRemaining}, interval:{LIMIT_Interval10sRemaining}, secondly:{LIMIT_SecondlyRemaining}");
                }

                if (response?.StatusCode == (HttpStatusCode)429 || response?.StatusCode == (HttpStatusCode)502 || exceptionMessage != null)
                {
                    attempts--;
                    ConsoleHelper.WriteLine(ConsoleColor.Magenta, $"HS request error {exceptionMessage ?? response?.StatusCode.ToString()}. Attempts left {attempts}");
                    Thread.Sleep(5000);
                }
            }
            while ((response?.StatusCode == (HttpStatusCode)429 || response?.StatusCode == (HttpStatusCode)502 || exceptionMessage != null) && attempts > 0);

            if (!response.IsSuccessStatusCode || exceptionMessage != null)
            {
                throw new Exception(response?.Content.ReadAsStringAsync().Result,
                    new Exception(exceptionMessage ?? ((int)(response?.StatusCode ?? 0)).ToString()));
            }

            // to check throttling
            //ConsoleHelper.WriteLine(ConsoleColor.DarkYellow,
            //    $"- HRRequest: dayly:{LIMIT_DailyRemaining}," +
            //    $" interval:{LIMIT_Interval10sRemaining}," +
            //    $" secondly:{LIMIT_SecondlyRemaining},");

            return await response.Content.ReadAsStringAsync();
        }

        public static bool IsDomainValid(string domain)
        {
            var result = true;

            if (string.IsNullOrWhiteSpace(domain))
            {
                result = false;
            }
            else if (domain.IndexOfAny(listOfIncorrectSymbolsForDomain.ToCharArray()) != -1)
            {
                result = false;
            }
            else
            {
                var domainLevels = domain.Split('.');
                string pattern = @"^[A-Za-z0-9]{2,30}$"; // // http://data.iana.org/TLD/tlds-alpha-by-domain.txt
                if (!Regex.IsMatch(domainLevels.Last(), pattern))
                {
                    result = false;
                }
            }

            if (!result)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Blue, $"Domain is not valid: {domain} --- skipped");
            }

            return result;
        }

        public static string NormalizeDomainName(string domain)
        {
            var result = domain.ToLower();

            if (result.EndsWith("/"))
                result = result.Remove(result.Length - 1);
            else
            if (result.EndsWith("."))
                result = result.Remove(result.Length - 1);

            if (result.StartsWith("https://www."))
                result = result.Remove(0, "https://www.".Length);
            else
            if (result.StartsWith("http://www."))
                result = result.Remove(0, "http://www.".Length);
            else
            if (result.StartsWith("https://"))
                result = result.Remove(0, "https://".Length);
            else
            if (result.StartsWith("www."))
                result = result.Remove(0, "www.".Length);

            if (result != domain)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Cyan, $"Domain {domain} normalized to {result}");
            }

            return result;
        }

    }

}
