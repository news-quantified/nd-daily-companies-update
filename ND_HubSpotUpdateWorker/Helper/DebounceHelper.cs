﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Net;
using System.IO;

namespace ND_HubSpotUpdateWorker.Helper
{
    public class DebounceHelper
    {
        public static DebounceItem GetDebounceItem(string email)
        {
            if (string.IsNullOrEmpty(email)) return null;

            DebounceItem item = null;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(string.Format("https://api.debounce.io/v1/?api=5cd979ec81501&email={0}", email));
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Method = "GET";

            try
            {
                WebResponse response = request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using StreamReader reader = new(stream);
                    string jsontext = reader.ReadToEnd();
                    DataContractJsonSerializer json = new(typeof(DebounceItem));
                    item = (DebounceItem)json.ReadObject(new MemoryStream(Encoding.Unicode.GetBytes(jsontext)));
                }
                response.Close();
            }
            catch (Exception exception)
            {
                ConsoleHelper.WriteLine(ConsoleColor.DarkGray, $"GetDebounceItem - exception: {exception.Message}");
            }

            return item;
        }
    }


    [DataContract]
    public class DebounceItem
    {
        [DataMember(Name = "debounce")]
        public debounce debounce { get; set; }
        [DataMember(Name = "success")]
        public int success { get; set; }
        [DataMember(Name = "balance")]
        public int balance { get; set; }
    }

    [DataContract]
    public class debounce
    {
        [DataMember(Name = "email")]
        public string email { get; set; }


        /// <summary>
        /// Syntax	1	Not an email.	Safe to send? - No
        /// Spam Trap	2	Spam-trap by ESPs. No
        /// Disposable	3	A temporary, disposable address. No
        /// Accept-All	4	A domain-wide setting. Maybe, Not recommended unless on private server
        /// Deliverable	5	Verified as real address. Yes
        /// Invalid 6	Verified as invalid (Bounce).	No
        /// Unknown	7	The server cannot be reached. No
        /// Role	8	Role accounts such as info, support, etc. Maybe, Not recommended
        /// </summary>
        [DataMember(Name = "code")]
        public int code { get; set; }
        [DataMember(Name = "role")]
        public bool role { get; set; }
        [DataMember(Name = "free_email")]
        public bool free_email { get; set; }
        [DataMember(Name = "result")]
        public string result { get; set; }
        [DataMember(Name = "reason")]
        public string reason { get; set; }
    }
}
