﻿using System;

namespace ND_HubSpotUpdateWorker.Helper
{
    public static class ConsoleHelper
    {
        public static readonly Func<string> DateTimeMark = () => DateTime.Now.ToString("hh:mm:ss.fff");

        private static readonly object _lock = new();

        public static void WriteLine(string message)
        {
            WriteLine(ConsoleColor.Gray, message);
        }

        public static void WriteLine(ConsoleColor color, string message)
        {
            WriteString(color, message, Console.WriteLine);
        }

        public static void Write(string message)
        {
            Write(ConsoleColor.Gray, message);
        }

        public static void Write(ConsoleColor color, string message)
        {
            WriteString(color, message, Console.Write);
        }

        private static void WriteString(ConsoleColor color, string message, Action<string> action)
        {
            lock (_lock)
            {
                Console.ForegroundColor = color;
                action.Invoke($"[{DateTimeMark.Invoke()}] {message}");
                Console.ResetColor();
            }
        }
    }
}
