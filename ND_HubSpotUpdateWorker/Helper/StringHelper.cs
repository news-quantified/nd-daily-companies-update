﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Helper
{
    public class StringHelper
    {
        public static string ClearString(string str)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(str.Trim())) return string.Empty;

            str = str.Replace("\"", "'")
                     .Replace("\r\n", string.Empty)
                     .Replace("\n", string.Empty)
                     .Replace("\r", string.Empty)
                     .Replace("\t", string.Empty)
                     .Replace("\\", "'")
                     .Replace("\u0003", string.Empty);

            return str;
        }
    }
}
