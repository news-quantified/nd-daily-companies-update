﻿using ND_HubSpotUpdateWorker.Model;
using System.Collections.Generic;

namespace ND_HubSpotUpdateWorker.Dto
{
    public class CompanyProcessContainer
    {
        public int logId { get; set; }
        public NDcrmMaster DBRecord { get; set; }
        public List<CompanySummaryItem> HSRecords { get; set; } = new(); // list of companies found by domain of DBRecord
        public List<string> AddedContactEmails { get; set; } = new();
        public List<string> NotAddedContactEmails { get; set; } = new(); // already exists

        public bool UpdatedHi { get; set; }
        public bool Created { get; set; }
        public bool UpdatedLow { get; set; }

        public string Error { get; set; }
    }
}
