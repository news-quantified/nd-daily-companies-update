﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Dto
{
    public class CompanyFromDB
    {
        public string domain { get; set; }
        public bool isAgency { get; set; }
        public bool isNYSE { get; set; }
        public bool isTopNews { get; set; }
        public bool isEAV { get; set; }
    }
}
