﻿
using ND_HubSpotUpdateWorker.Attributes;

namespace ND_HubSpotUpdateWorker.Dto
{
    public class CompanySummaryItem
    {
        [NotUpdate]
        public long companyId { get;set; }


        #region low priority update
        [Update_IfNotEmpty]
        public string annualrevenue { get; set; }
        [Update_IfNotEmpty]
        public string city { get; set; }
        [Update_IfNotEmpty]
        public string company_email { get; set; }
        [Update_IfNotEmpty]
        public string company_type { get; set; }
        [Update_IfNotEmpty]
        public string country { get; set; }
        [Update_IfNotEmpty]
        public string description { get; set; }
        [Update_IfNotEmpty]
        public string industry { get; set; }
        [Update_IfNotEmpty]
        public string name { get; set; }
        [Update_IfNotEmpty]
        public string numberofemployees { get; set; }
        [Update_IfNotEmpty]
        public string phone { get; set; }
        [Update_IfNotEmpty]
        public string public_company_exchange { get; set; }
        [Update_IfNotEmpty]
        public string related_agency { get; set; }
        [Update_IfNotEmpty]
        public string state { get; set; }
        [Update_IfNotEmpty]
        public string address { get; set; }
        [Update_IfNotEmpty]
        public string address2 { get; set; }
        [Update_IfNotEmpty]
        public string ticker_symbol { get; set; }
        [Update_IfNotEmpty]
        public string timezone { get; set; }
        [Update_IfNotEmpty]
        public string total_money_raised { get; set; }
        [Update_IfNotEmpty]
        public string website { get; set; }
        [Update_IfNotEmpty]
        public string market_cap { get; set; }

        public bool is_public { get; set; }
        [Update_IfNotEmpty]
        public string founded_year { get; set; }
        [Update_IfNotEmpty]
        public string facebook_company_page { get; set; }
        [Update_IfNotEmpty]
        public string facebookfans { get; set; }
        [Update_IfNotEmpty]
        public string googleplus_page { get; set; }
        [Update_IfNotEmpty]
        public string linkedin_company_page { get; set; }
        [Update_IfNotEmpty]
        public string twitterbio { get; set; }
        [Update_IfNotEmpty]
        public string twitterfollowers { get; set; }
        [Update_IfNotEmpty]
        public string twitterhandle { get; set; }
        [Update_IfNotEmpty]
        public string investor_relations_site { get; set; }
        [Update_IfNotEmpty]
        public string newsroom_url { get; set; }
        [Update_IfNotEmpty]
        public string competitor { get; set; }
        
        public long current_year_billing { get; set; }

        [Update_IfNotEmpty]
        public string word_count { get; set; }

        [Update_IfNotEmpty]
        public string public_company_dashboard_nq_ { get; set; }

        [Update_IfNotEmpty]
        public long hubspot_owner_id { get; set; }

        #endregion #region low priority update


        #region hi priority update

        [Update_Hi]
        [Update_IfNotEmpty]
        public string domain { get; set; }

        [Update_Hi]
        [Update_IfNotEmpty]
        public string working_with_competitor { get; set; }

        [Update_Hi]
        [Update_IfNotEmpty]
        public string distributes_earnings { get; set; }
        
        [Update_Hi]
        public long release_count_current_year_ { get; set; }

        [Update_Hi]
        public long release_count_prior_year_ { get; set; }

        [Update_Hi]
        public double eav_high_current_year_ { get; set; }

        [Update_Hi]
        public double eav_high_prior_year_ { get; set; }

        [Update_Hi]
        public double eav_low_current_year_ { get; set; }

        [Update_Hi]
        public double eav_low_prior_year_ { get; set; }
        
        [Update_Hi]
        [Update_IfNotEmpty]
        public string word_count_average_ { get; set; }

        [Update_Hi]
        [Update_IfNotEmpty]
        public string multimedia_sender { get; set; }

        [Update_Hi]
        [Update_IfNotEmpty]
        public string static_media_sender { get; set; }

        [Update_Hi]
        [Update_IfNotEmpty]
        public string amazon_affiliate_links { get; set; }

        [Update_Hi]
        public long static_media_count_current_year_ { get; set; }

        [Update_Hi]
        public long static_media_count_previous_year_ { get; set; }

        [Update_Hi]
        public long multimedia_count_current_year_ { get; set; }

        [Update_Hi]
        public long multimedia_count_previous_year_ { get; set; }

        [Update_Hi]
        public double ecv_prior_year_ { get; set; }

        [Update_Hi]
        public double ecv_current_year_ { get; set; }

        [Update_Hi]
        [Update_IfNotEmpty]
        public string prospect_level { get; set; }

        #endregion hi priority update

        // for internal usage only
        [NotUpdate]
        public string json { get; set; }



        public CompanySummaryItem HistoricalClone;

        [NotUpdate]
        public CompanySummaryItem Clone => (CompanySummaryItem)MemberwiseClone();
    }
}
