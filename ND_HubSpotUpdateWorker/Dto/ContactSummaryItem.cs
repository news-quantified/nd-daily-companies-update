﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Dto
{
    public class ContactSummaryItem
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string company { get; set; }
        public string jobtitle { get; set; }
        public string phone { get; set; }
        public string nq_source { get; set; }
        public DateTime? nq_contact_date { get; set; }

        public string full_name { get; set; }
        public string location { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string linkedin { get; set; }
        public string employment_role { get; set; }
        public string employment_subrole { get; set; }
        public string seniority { get; set; }
        public string bio { get; set; }

        public string contact_id { get; set; } // internal usage
    }
}
