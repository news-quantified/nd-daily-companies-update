﻿using ND_HubSpotUpdateWorker.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Dto
{
    public class ContactFromDB : ParserNewswire
    {
        public int CompleteResult
        {
            get
            {
                int res = 0;

                if (!string.IsNullOrEmpty(Name)) res++;
                if (!string.IsNullOrEmpty(CompanyName)) res++;
                if (!string.IsNullOrEmpty(Title)) res++;
                if (!string.IsNullOrEmpty(Phone)) res++;
                return res;
            }
        }
    }
}
