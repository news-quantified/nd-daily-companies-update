﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ND_HubSpotUpdateWorker.Extensions
{
    public static class ExpressionExtension
    {
        public static Expression<Func<T, bool>> CombineAndAlso<T>(this Expression<Func<T, bool>> e1, Expression<Func<T, bool>> e2)
        {
            return Expression.Lambda<Func<T, bool>>(
                Expression.AndAlso(e1.Body, new ExpressionMapper(e2.Parameters, e1.Parameters).Visit(e2.Body)),
                e1.Parameters);
        }

        public static Expression<Func<T, bool>> CombineOrElse<T>(this Expression<Func<T, bool>> e1, Expression<Func<T, bool>> e2)
        {
            return Expression.Lambda<Func<T, bool>>(
                Expression.OrElse(e1.Body, new ExpressionMapper(e2.Parameters, e1.Parameters).Visit(e2.Body)),
                e1.Parameters);
        }

        private class ExpressionMapper : ExpressionVisitor
        {
            private IDictionary<ParameterExpression, ParameterExpression> Map { get; set; }

            public ExpressionMapper(IList<ParameterExpression> from, IList<ParameterExpression> to)
            {
                Map = new Dictionary<ParameterExpression, ParameterExpression>();

                for (int i = 0; i != from.Count && i != to.Count; i++)
                {
                    Map.Add(from[i], to[i]);
                }
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                if (Map.TryGetValue(node, out ParameterExpression expression))
                {
                    node = expression;
                }
                    
                return base.VisitParameter(node);
            }
        }
    }
}
