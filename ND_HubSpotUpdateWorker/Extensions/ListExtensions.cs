﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Extensions
{
    public static class ListExtensions
    {
        public static IEnumerable<IEnumerable<TSource>> ChunkBy<TSource>
            (this IEnumerable<TSource> source, int chunkSize)
        {
            while (source.Any())                     // while there are elements left
            {   // still something to chunk:
                yield return source.Take(chunkSize); // return a chunk of chunkSize
                source = source.Skip(chunkSize);     // skip the returned chunk
            }
        }

        public static string Join(this IEnumerable<string> list, string separator = ",")
        {
            return string.Join(separator, list);
        }
    }
}
