﻿using ND_HubSpotUpdateWorker.Dto;
using ND_HubSpotUpdateWorker.Extensions;
using ND_HubSpotUpdateWorker.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Core
{
    /// <summary>
    /// update hi-priority data of existed companies
    /// </summary>
    public static class Stage1Updater
    {
        public static void Run(List<CompanyProcessContainer> companyList)
        {
            var companyListPrioritized = companyList.Where(x => !x.DBRecord.ND_HubSpotPrioritized.HasValue ||
                    (x.DBRecord.ND_HubSpotPrioritized.HasValue && x.DBRecord.LastRelease.HasValue && x.DBRecord.LastRelease.Value > x.DBRecord.ND_HubSpotPrioritized.Value) ||
                    (x.DBRecord.ND_HubSpotPrioritized.HasValue && x.DBRecord.LastUpdate.HasValue && x.DBRecord.LastUpdate.Value > x.DBRecord.ND_HubSpotPrioritized.Value))
                .ToList();

            ConsoleHelper.WriteLine(ConsoleColor.White, $" +++ Stage 1 started. Records count: {companyListPrioritized.Count}");

            Interlocked.Exchange(ref Globals.COUNT_STAGE1_TOTAL, companyListPrioritized.Count);

            Parallel.ForEach(companyListPrioritized.ChunkBy(100), new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, chunk =>
            {
                Interlocked.Add(ref Globals.COUNT_STAGE1_PROCESSED, chunk.Count());
                BatchProcess(chunk);
            });

            ConsoleHelper.WriteLine(ConsoleColor.White, $" +++ Stage 1 finished. Records processed: {companyListPrioritized.Where(x => x.UpdatedHi).Count()}");
        }


        static void BatchProcess(IEnumerable<CompanyProcessContainer> companyProcessList)
        {
            try
            {
                Parallel.ForEach(companyProcessList, new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, company =>
                {
                    if (!HubSpotHelper.IsDomainValid(company.DBRecord.CompanyUrl))
                    {
                        company.Error = $"Invalid domain: {company.DBRecord.CompanyUrl}";
                        DBFunc.GetIdForLog(company.DBRecord.CompanyUrl, company.Error);
                        Interlocked.Increment(ref Globals.COUNT_COMPANY_UPDATE_ERROR_HI);
                    }
                    else
                    {
                        try
                        {
                            company.HSRecords = HubSpotHelper.GetCompanyFromHubSpotByDomain(company.DBRecord.CompanyUrl.ToLower());
                        }
                        catch (Exception ex)
                        {
                            company.Error = ex.ToString();
                        }

                        if (company.HSRecords.Count > 1)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.DarkCyan,
                                $"Found {company.HSRecords.Count} HS companies by one domain: " +
                                $"{company.HSRecords.Select(x => x.domain).Join()}");
                        }

                        MapCompanyDBToHS_HiPriority(company);
                        company.HSRecords.ForEach(x => {
                            HubSpotHelper.RecheckCompanyBeforeUpdate(x);
                            x.json = HubSpotHelper.CompanyToJson(x, false);
                        });
                    }
                });

                var companyProcessListFiltered = companyProcessList.Where(x => x.HSRecords.Count > 0 && x.Error == null).ToList();
                DBFunc.GetIdsForLog(companyProcessListFiltered, "Log added Hi");

                try
                {
                    HubSpotHelper.UpdateGroupOfCompaniesToHubSpot(companyProcessListFiltered);
                    DBFunc.CompanyListAddedToHS(companyProcessListFiltered, "Update Hi");

                    companyProcessListFiltered.ForEach(x => x.UpdatedHi = true);
                    Interlocked.Add(ref Globals.COUNT_COMPANY_UPDATE_HI, companyProcessListFiltered.Count);

                    ConsoleHelper.WriteLine(ConsoleColor.Green, $"Batch ({companyProcessListFiltered.Count} companies) of Domains updated - OK");
                    DBFunc.UpdatePrioritizedFieldsInNDcrmMasters(companyProcessListFiltered.Select(x => x.DBRecord));
                }
                catch //(Exception ex1)
                {
                    var companyProcessListChunked = companyProcessListFiltered
                        .ChunkBy(Math.Max(companyProcessListFiltered.Count / 10, 1));

                    ConsoleHelper.WriteLine(ConsoleColor.Magenta, $" - Chunks of Batch processing");
                    Parallel.ForEach(companyProcessListChunked, new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, chunk =>
                    {
                        try
                        {
                            HubSpotHelper.UpdateGroupOfCompaniesToHubSpot(chunk.ToList());
                            DBFunc.CompanyListAddedToHS(companyProcessListFiltered, "Update Hi");

                            chunk.ToList().ForEach(x => x.UpdatedHi = true);
                            Interlocked.Add(ref Globals.COUNT_COMPANY_UPDATE_HI, chunk.Count());

                            ConsoleHelper.WriteLine(ConsoleColor.Green, $"- Chunk ({chunk.Count()}) of Batch updated - OK");
                            DBFunc.UpdatePrioritizedFieldsInNDcrmMasters(chunk.Select(x => x.DBRecord));
                        }
                        catch //(Exception ex2)
                        {
                            foreach (var company in chunk)
                            {
                                try
                                {
                                    var res = HubSpotHelper.UpdateCompanyToHubSpot(company, true);
                                    DBFunc.CompanyAddedToHS(company.logId, "Update Hi",
                                        company.HSRecords.Select(x => x.json).Join(),
                                        res.Join());

                                    DBFunc.UpdatePrioritizedFieldsInNDcrmMasters(company.DBRecord);

                                    company.UpdatedHi = true;
                                    Interlocked.Increment(ref Globals.COUNT_COMPANY_UPDATE_HI);
                                    ConsoleHelper.WriteLine(ConsoleColor.Green, string.Format("Domain : {0} updated - OK", company.DBRecord.CompanyUrl));
                                }
                                catch (Exception ex3)
                                {
                                    DBFunc.CompanyNotAddedToHS(company.logId, "Update Hi", company.HSRecords.Select(x => x.json).Join(), ex3.Message);
                                    Interlocked.Increment(ref Globals.COUNT_COMPANY_UPDATE_ERROR_HI);
                                    ConsoleHelper.WriteLine(ConsoleColor.Cyan, string.Format("Domain : {0} Update - Exeption - {1}", company.DBRecord.CompanyUrl, ex3.Message));
                                }
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Cyan, string.Format("Company batch GENERAL ESCEPTION : {0}", ex.Message));
            }
        }

        static void MapCompanyDBToHS_HiPriority(CompanyProcessContainer companyProcess)
        {
            foreach (var CompanyItemHS in companyProcess.HSRecords)
            {
                var crm = companyProcess.DBRecord;
                if (crm != null)
                {
                    CompanyItemHS.HistoricalClone = CompanyItemHS.Clone;

                    CompanyItemHS.amazon_affiliate_links = (crm.AmazonLinks.HasValue && crm.AmazonLinks.Value == true) ? "Yes" : "No";
                    if (!string.IsNullOrEmpty(crm.PIDs)) CompanyItemHS.competitor = crm.PIDs;
                    CompanyItemHS.distributes_earnings = (crm.Earnings.HasValue && crm.Earnings.Value == true) ? "Yes" : "No";
                    CompanyItemHS.eav_high_current_year_ = crm.EAVHighCurrYear.HasValue ? Math.Round(crm.EAVHighCurrYear.Value, 0) : 0.0;
                    CompanyItemHS.eav_high_prior_year_ = crm.EAVHighPrevYear.HasValue ? Math.Round(crm.EAVHighPrevYear.Value, 0) : 0.0;
                    CompanyItemHS.eav_low_current_year_ = crm.EAVLowCurrYear.HasValue ? Math.Round(crm.EAVLowCurrYear.Value, 0) : 0.0;
                    CompanyItemHS.eav_low_prior_year_ = crm.EAVLowPrevYear.HasValue ? Math.Round(crm.EAVLowPrevYear.Value, 0) : 0.0;
                    CompanyItemHS.multimedia_sender = (crm.Multimedia_Sender.HasValue && crm.Multimedia_Sender.Value == true) ? "Yes" : "No";
                    CompanyItemHS.release_count_current_year_ = crm.CurrYearReleases ?? 0;
                    CompanyItemHS.release_count_prior_year_ = crm.PrevYearReleases ?? 0;
                    CompanyItemHS.static_media_sender = (crm.StaticMedia_Sender.HasValue && crm.StaticMedia_Sender.Value == true) ? "Yes" : "No";
                    if (crm.StoryCount.HasValue) CompanyItemHS.word_count_average_ = Math.Round(crm.StoryCount.Value, 0).ToString();
                    CompanyItemHS.static_media_count_current_year_ = crm.StaticMediaCount_Curr ?? 0;
                    CompanyItemHS.static_media_count_previous_year_ = crm.StaticMediaCount_Prev ?? 0;
                    CompanyItemHS.multimedia_count_current_year_ = crm.MultiMediaCount_Curr ?? 0;
                    CompanyItemHS.multimedia_count_previous_year_ = crm.MultiMediaCount_Prev ?? 0;
                    CompanyItemHS.ecv_current_year_ = crm.ND_ECVCurrYear ?? 0;
                    CompanyItemHS.ecv_prior_year_ = crm.ND_ECVPrevYear ?? 0;
                    CompanyItemHS.prospect_level = crm.prospect_level ?? string.Empty;
                }
            }
        }
    }
}
