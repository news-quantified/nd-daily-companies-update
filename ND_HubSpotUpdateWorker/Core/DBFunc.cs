﻿
using Microsoft.EntityFrameworkCore;
using ND_HubSpotUpdateWorker.Dto;
using ND_HubSpotUpdateWorker.Extensions;
using ND_HubSpotUpdateWorker.Helper;
using ND_HubSpotUpdateWorker.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Core
{
    public class DBFunc
    {
        public static async Task<List<NDcrmMaster>> GetCompaniesForUpdateAsync(string domain = null)
        {
            List<NDcrmMaster> list = new();

            try
            {
                using var db = new NQDBContext();

                if (!string.IsNullOrEmpty(domain))
                {
                    list = await db.NDcrmMasters.AsNoTracking()
                        .Where(n => n.CompanyUrl.ToLower() == domain)
                        .ToListAsync();

                    list.ForEach(x => {
                        x.ND_HubSpot = null;
                        x.ND_HubSpotPrioritized = null;
                    });
                }
                else
                {
                    list = await db.NDcrmMasters.Where(n =>
                                            (!n.ND_HubSpot.HasValue
                                              || (n.ND_HubSpot.HasValue && n.LastRelease.HasValue && n.LastRelease.Value > n.ND_HubSpot.Value)
                                              || (n.ND_HubSpot.HasValue && n.LastUpdate.HasValue && n.LastUpdate.Value > n.ND_HubSpot.Value))
                                            && n.CompanyUrl.Contains("."))
                                            .OrderByDescending(n => n.CompanyType)
                                            .ToListAsync();
                }

                list.ForEach(c => c.CompanyUrl = c.CompanyUrl.ToLower());
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetCompaniesForUpdate exception: {ex.Message}");
                MailHelper.SendEmails("DBFunc", "GetCompaniesForUpdate_EX", ex.Message);
            }

            return list;
        }

        public static void UpdatePrioritizedFieldsInNDcrmMasters(NDcrmMaster record)
        {
            UpdatePrioritizedFieldsInNDcrmMasters(new List<NDcrmMaster> { record });
        }

        public static void UpdatePrioritizedFieldsInNDcrmMasters(IEnumerable<NDcrmMaster> records)
        {
            var sw = Stopwatch.StartNew();
            var dt = DateTime.Now;
            try
            {
                Globals.DbSemaphore.Wait();
                try
                {
                    using var db = new NQDBContext();
                    foreach (var record in records)
                    {
                        db.Attach(record);
                        record.ND_HubSpotPrioritized = dt;
                    }

                    db.SaveChanges();
                }
                finally
                {
                    Globals.DbSemaphore.Release();
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"UpdatePrioritizedFieldsInNDcrmMasters exception: {ex}");
            }
            ConsoleHelper.WriteLine(ConsoleColor.White, $"UpdatePrioritizedFieldsInNDcrmMasters {sw.Elapsed}");
        }

        public static void UpdateFieldsInNDcrmMasters(NDcrmMaster record)
        {
            UpdateFieldsInNDcrmMasters(new List<NDcrmMaster> { record });
        }

        public static void UpdateFieldsInNDcrmMasters(IEnumerable<NDcrmMaster> records)
        {
            var sw = Stopwatch.StartNew();
            var dt = DateTime.Now;
            try
            {
                Globals.DbSemaphore.Wait();
                try
                {
                    using var db = new NQDBContext();
                    foreach (var record in records)
                    {
                        db.Attach(record);
                        record.ND_HubSpot = dt;
                    }

                    db.SaveChanges();
                }
                finally
                {
                    Globals.DbSemaphore.Release();
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"UpdateFieldsInNDcrmMasters exception: {ex}");
            }
            ConsoleHelper.WriteLine(ConsoleColor.White, $"UpdateFieldsInNDcrmMasters {sw.Elapsed}");
        }

        public static int GetIdForLog(string domain, string errorMessage = "Log added")
        {
            int id = 0;

            try
            {
                Globals.DbSemaphore.Wait();
                try
                {
                    using var db = new NQDBContext();

                    ND_UTility_HS_LogCompany log = new()
                    {
                        AccountId = 5691062,
                        CompanyDomain = domain,
                        CompanyHSId = 0,
                        DateTimeUpdated = DateTime.Now,
                        ErrorMessage = errorMessage,
                        isSuccess = false,
                    };

                    db.ND_UTility_HS_LogCompanies.Add(log);
                    db.SaveChanges();
                    id = log.Id;
                }
                finally
                {
                    Globals.DbSemaphore.Release();
                }
            }
            catch(Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetIdForLog exception: {ex}");
            }

            return id;
        }

        public static void GetIdsForLog(IEnumerable<CompanyProcessContainer> companyProcessList, string errorMessagePrefix)
        {
            var entityList = new List<ND_UTility_HS_LogCompany>();
            var now = DateTime.Now;
            foreach (var process in companyProcessList)
            {
                if (process.logId > 0)
                    continue;

                entityList.Add(new ND_UTility_HS_LogCompany()
                {
                    AccountId = 5691062,
                    CompanyDomain = process.DBRecord.CompanyUrl,
                    CompanyHSId = (int)(process.HSRecords.FirstOrDefault()?.companyId ?? 0), // TODO: long to int conversion!
                    DateTimeUpdated = now,
                    FoundInHS = process.HSRecords.Count > 0,
                    InputJSON = process.HSRecords.FirstOrDefault()?.json,
                    ErrorMessage = errorMessagePrefix + (process.HSRecords.Count > 1 ? $" / Number of companies found - {process.HSRecords.Count}" : ""),
                    isSuccess = false,
                });
            }

            if (entityList.Count > 0)
            {
                try
                {
                    Globals.DbSemaphore.Wait();
                    try
                    {
                        using var db = new NQDBContext();
                        db.ND_UTility_HS_LogCompanies.AddRange(entityList);
                        db.SaveChanges();
                    }
                    finally
                    {
                        Globals.DbSemaphore.Release();
                    }

                    foreach (var process in companyProcessList)
                    {
                        process.logId = entityList.FirstOrDefault(x => x.CompanyDomain == process.DBRecord.CompanyUrl)?.Id ?? process.logId;
                    }
                }
                catch (Exception ex)
                {
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetIdsForLog exception: {ex}");
                }
            }
        }

        public static void CompanyAddedToHS(int logId, string action, string inputJson, string response)
        {
            try
            {
                Globals.DbSemaphore.Wait();
                try
                {
                    using var db = new NQDBContext();
                    var log = db.ND_UTility_HS_LogCompanies.FirstOrDefault(l => l.Id == logId);
                    if (log != null)
                    {
                        log.isSuccess = true;
                        log.ActionType = action;
                        log.DateTimeUpdated = DateTime.Now;
                        log.ErrorMessage = response;
                        log.InputJSON = inputJson;
                        db.SaveChanges();
                    }
                }
                finally
                {
                    Globals.DbSemaphore.Release();
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"CompanyAddedToHS exception: {ex}");
            }
        }

        public static void CompanyListAddedToHS(IEnumerable<CompanyProcessContainer> list, string action)
        {
            var success = false;
            var attempts = 10;

            do
            {
                attempts--;

                try
                {
                    var logIds = list.Select(x => x.logId).ToList();
                    Globals.DbSemaphore.Wait();

                    try
                    {
                        var now = DateTime.Now;

                        using var db = new NQDBContext();
                        var logList = db.ND_UTility_HS_LogCompanies.Where(x => logIds.Contains(x.Id)).ToList();

                        foreach (var log in logList)
                        {
                            log.isSuccess = true;
                            log.ActionType = action;
                            log.DateTimeUpdated = now;
                            //log.ErrorMessage = response; // TODO
                            log.InputJSON = list.Where(x => x.logId == log.Id)
                                .SelectMany(x => x.HSRecords)
                                .Select(x => x.json)
                                .Join();
                        }

                        db.SaveChanges();
                        success = true;
                    }
                    finally
                    {
                        Globals.DbSemaphore.Release();
                    }
                }
                catch (Exception ex)
                {
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"CompanyListAddedToHS exception: {ex.Message}");
                    if (attempts <= 0)
                        break; //throw;
                    else
                        ConsoleHelper.WriteLine(ConsoleColor.Blue, $"CompanyListAddedToHS retry query. Attempts left - {attempts - 1}");
                }
            }
            while (!success && attempts > 0);
        }

        public static void CompanyAddedContact(int logId, string email)
        {
            try
            {
                Globals.DbSemaphore.Wait();
                try
                {
                    using var db = new NQDBContext();
                    var log = db.ND_UTility_HS_LogCompanies.FirstOrDefault(l => l.Id == logId);
                    if (log != null)
                    {
                        if (!string.IsNullOrEmpty(log.AddedContacts))
                            log.AddedContacts = log.AddedContacts + ";" + email;
                        else
                            log.AddedContacts = email;

                        log.isSuccess = true;
                        log.DateTimeUpdated = DateTime.Now;

                        db.SaveChanges();
                    }
                }
                finally
                {
                    Globals.DbSemaphore.Release();
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"CompanyAddedContacts exception: {ex}");
            }
        }



        public static void CompanyNotAddedToHS(int logId, string action, string inputJson, string errorJson)
        {
            try
            {
                Globals.DbSemaphore.Wait();
                try
                {
                    using var db = new NQDBContext();
                    var log = db.ND_UTility_HS_LogCompanies.FirstOrDefault(l => l.Id == logId);
                    if (log != null)
                    {
                        log.isSuccess = false;
                        log.ActionType = action;
                        log.DateTimeUpdated = DateTime.Now;
                        log.ErrorMessage = errorJson;
                        log.InputJSON = inputJson;
                        db.SaveChanges();
                    }
                }
                finally
                {
                    Globals.DbSemaphore.Release();
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"CompanyNotAddedToHS exception: {ex}");
            }
        }


        public static CompanySummaryItem GetAllFieldsForCompanyFromDB(string domain)
        {
            CompanySummaryItem company = new()
            {
                domain = domain.ToLower()
            };

            var success = false;
            var attempts = 10;

            do
            {
                attempts--;

                try
                {
                    using var db = new NQDBContext();
                    
                    var cl = db.ClearbitCompanyAPIs.FirstOrDefault(c => c.domain.ToLower() == domain.ToLower());
                    if (cl != null)
                    {
                        if (cl.metrics_annualRevenue.HasValue)
                        {
                            company.annualrevenue = cl.metrics_annualRevenue.Value.ToString();
                        }
                        else if (!string.IsNullOrEmpty(cl.metrics_estimatedAnnualRevenue))
                        {
                            company.annualrevenue = cl.metrics_estimatedAnnualRevenue;
                        }

                        if (!string.IsNullOrEmpty(cl.email)) company.company_email = cl.email;
                        if (!string.IsNullOrEmpty(cl.geo_city)) company.city = cl.geo_city;
                        if (!string.IsNullOrEmpty(cl.geo_country)) company.country = cl.geo_country;
                        if (!string.IsNullOrEmpty(cl.description)) company.description = cl.description;
                        if (!string.IsNullOrEmpty(cl.category_industryGroup)) company.industry = cl.category_industryGroup;
                        if (!string.IsNullOrEmpty(cl.name)) company.name = cl.name;
                        if (cl.employees.HasValue) company.numberofemployees = cl.employees.Value.ToString();
                        if (!string.IsNullOrEmpty(cl.phone)) company.phone = cl.phone;
                        if (!string.IsNullOrEmpty(cl.geo_stateCode)) company.state = cl.geo_stateCode;
                        company.address = string.Format("{0}{1}", string.IsNullOrEmpty(cl.geo_streetNumber) ? string.Empty : cl.geo_streetNumber + " ",
                                                                    string.IsNullOrEmpty(cl.geo_streetName) ? string.Empty : cl.geo_streetName);
                        if (!string.IsNullOrEmpty(cl.geo_subPremise)) company.address2 = cl.geo_subPremise;
                        if (!string.IsNullOrEmpty(cl.timeZone)) company.timezone = cl.timeZone;
                        if (cl.raised.HasValue) company.total_money_raised = cl.raised.Value.ToString();
                        if (!string.IsNullOrEmpty(cl.site_url)) company.website = cl.site_url;
                        if (cl.foundedYear.HasValue) company.founded_year = cl.foundedYear.Value.ToString();
                        if (!string.IsNullOrEmpty(cl.facebook_handle)) company.facebook_company_page = cl.facebook_handle;
                        if (cl.facebook_likes.HasValue) company.facebookfans = cl.facebook_likes.Value.ToString();
                        if (!string.IsNullOrEmpty(cl.googleplus_handle)) company.googleplus_page = cl.googleplus_handle;
                        if (!string.IsNullOrEmpty(cl.linkedin_handle)) company.linkedin_company_page = cl.linkedin_handle;
                        if (!string.IsNullOrEmpty(cl.twitter_site)) company.twitterbio = cl.twitter_site;
                        if (cl.twitter_followers.HasValue) company.twitterfollowers = cl.twitter_followers.Value.ToString();
                        if (!string.IsNullOrEmpty(cl.twitter_handle)) company.twitterhandle = cl.twitter_handle;
                    }
                    
                    company.working_with_competitor = db.NDNews.Any(n => !string.IsNullOrEmpty(n.Domains) && n.Domains.ToLower().Contains(domain.ToLower())) ? "Yes" : "No";
                        
                    var crm = db.NDcrmMasters.FirstOrDefault(n => !string.IsNullOrEmpty(n.CompanyUrl) && n.CompanyUrl == domain);
                    if (crm != null)
                    {
                        if (!string.IsNullOrEmpty(crm.PIDs)) company.competitor = crm.PIDs;
                        company.distributes_earnings = (crm.Earnings.HasValue && crm.Earnings.Value == true) ? "Yes" : "No";
                        company.multimedia_sender = (crm.Multimedia_Sender.HasValue && crm.Multimedia_Sender.Value == true) ? "Yes" : "No";
                        if (crm.StoryCount.HasValue) company.word_count_average_ = Math.Round(crm.StoryCount.Value, 0).ToString();
                        company.amazon_affiliate_links = (crm.AmazonLinks.HasValue && crm.AmazonLinks.Value == true) ? "Yes" : "No";
                        if (!string.IsNullOrEmpty(crm.AgencyUrl)) company.related_agency = crm.AgencyUrl;
                        if (!string.IsNullOrEmpty(crm.CompanyType) && crm.CompanyType.ToLower() == "agency") company.company_type = "PR Agency";
                        company.eav_high_current_year_ = crm.EAVHighCurrYear.HasValue ? Math.Round(crm.EAVHighCurrYear.Value, 0) : 0.0;
                        company.eav_high_prior_year_ = crm.EAVHighPrevYear.HasValue ? Math.Round(crm.EAVHighPrevYear.Value, 0) : 0.0;
                        company.eav_low_current_year_ = crm.EAVLowCurrYear.HasValue ? Math.Round(crm.EAVLowCurrYear.Value, 0) : 0.0;
                        company.eav_low_prior_year_ = crm.EAVLowPrevYear.HasValue ? Math.Round(crm.EAVLowPrevYear.Value, 0) : 0.0;
                        company.release_count_prior_year_ = crm.PrevYearReleases ?? 0;
                        company.release_count_current_year_ = crm.CurrYearReleases ?? 0;

                        company.static_media_count_current_year_ = crm.StaticMediaCount_Curr ?? 0;
                        company.static_media_count_previous_year_ = crm.StaticMediaCount_Prev ?? 0;
                        company.multimedia_count_current_year_ = crm.MultiMediaCount_Curr ?? 0;
                        company.multimedia_count_previous_year_ = crm.MultiMediaCount_Prev ?? 0;

                        company.static_media_sender = (crm.StaticMedia_Sender.HasValue && crm.StaticMedia_Sender.Value == true) ? "Yes" : "No";
                        company.ecv_prior_year_ = crm.ND_ECVPrevYear ?? 0;
                        company.ecv_current_year_ = crm.ND_ECVCurrYear ?? 0;
                        company.hubspot_owner_id = crm.hubspot_owner_id ?? 0;
                        company.prospect_level = crm.prospect_level ?? string.Empty;
                        company.public_company_dashboard_nq_ = crm.PublicCompanyDashboardURL ?? string.Empty;
                    }
                    
                    if (!company.is_public)
                    {
                        var f4 = db.Fundamental4.FirstOrDefault(f => !string.IsNullOrEmpty(f.URL) && f.URL.ToLower().Contains(domain.ToLower()));
                        if (f4 != null)
                        {
                            if (string.IsNullOrEmpty(company.ticker_symbol) && !string.IsNullOrEmpty(f4.Symbol))
                            {
                                company.ticker_symbol = f4.Symbol;
                                company.is_public = true;
                            }
                            if (string.IsNullOrEmpty(company.public_company_exchange) && !string.IsNullOrEmpty(f4.Exchange)) company.public_company_exchange = f4.Exchange;
                        }
                    }
                        
                    if (company.is_public && !string.IsNullOrEmpty(company.ticker_symbol) && !string.IsNullOrEmpty(company.public_company_exchange))
                    {
                        var ff = db.Fundamentals.Where(f => f.Symbol.ToLower() == company.ticker_symbol.ToLower() && f.Exchange.ToLower() == company.public_company_exchange.ToLower())
                                                .OrderByDescending(f => f.JDate)
                                                .Take(1)
                                                .FirstOrDefault();

                        if (ff != null)
                        {
                            company.market_cap = (ff.MktCap.HasValue) ? ff.MktCap.Value.ToString() : string.Empty;
                        }
                    }
                    
                    success = true;
                }
                catch (Exception ex)
                {
                    ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForCompanyFromDB exception: {ex.Message}");
                    if (attempts <= 0)
                        throw;
                    else
                        ConsoleHelper.WriteLine(ConsoleColor.Blue, $"GetAllFieldsForCompanyFromDB retry query. Attempts left - {attempts-1}");
                }
            }
            while (!success && attempts > 0);

            if (company != null)
            {
                if (!string.IsNullOrEmpty(company.description)) company.description = StringHelper.ClearString(company.description);
                if (!string.IsNullOrEmpty(company.name)) company.name = StringHelper.ClearString(company.name);
                if (!string.IsNullOrEmpty(company.phone)) company.phone = StringHelper.ClearString(company.phone);
            }

            return company;
        }

        public static List<CompanySummaryItem> GetAllFieldsForDomainListForCompanyFromDB(List<string> domains)
        {
            ConsoleHelper.WriteLine(ConsoleColor.White, $"GetAllFieldsForDomainListForCompanyFromDB started. Count:{domains.Count}");

            domains = domains.Select(x => x.ToLower()).ToList();
            var result = new List<CompanySummaryItem>();
            object lockObject = new();

            try
            {
                List<ClearbitCompanyAPI> clearbitCompanyAPIsList = null;
                List<string> NDNewsDomains = null;
                List<NDcrmMaster> NDcrmMasterList = null;
                List<Fundamental4> Fundamental4List = null;

                // TODO: make appropriate joins instead
                Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism },
                    () => {
                        try
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.White, $"GetAllFields - ClearbitCompanyAPIs receiving");

                            using var dbNQ = new NQDBContext();
                            clearbitCompanyAPIsList = dbNQ.ClearbitCompanyAPIs
                             .Where(c => domains.Contains(c.domain.ToLower()))
                             .ToList();

                            ConsoleHelper.WriteLine(ConsoleColor.White, $"GetAllFields - ClearbitCompanyAPIs received: {clearbitCompanyAPIsList.Count}");
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (ClearbitCompanyAPIs) exception: {ex.Message}");
                        }
                        
                    },
                    () => {
                        try
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Blue, $"GetAllFields - NDNews receiving");

                            using var dbNQ = new NQDBContext();
                            NDNewsDomains = dbNQ.NDNews
                             .Where(n => !string.IsNullOrEmpty(n.Domains) &&
                                         domains.Any(d => n.Domains.ToLower().Contains(d)))
                             .Select(x => x.Domains)
                             .ToList();

                            ConsoleHelper.WriteLine(ConsoleColor.Blue, $"GetAllFields - NDNews received: {NDNewsDomains.Count}");
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (NDNews) exception: {ex.Message}");
                        }
                    },
                    () => {
                        try
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Green, $"GetAllFields - NDcrmMasters receiving");

                            using var dbNQ = new NQDBContext();
                            NDcrmMasterList = dbNQ.NDcrmMasters
                                    .Where(n => !string.IsNullOrEmpty(n.CompanyUrl) && domains.Contains(n.CompanyUrl.ToLower()))
                                    .ToList();

                            ConsoleHelper.WriteLine(ConsoleColor.Green, $"GetAllFields - NDcrmMasters received: {NDcrmMasterList.Count}");
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (NDcrmMasters) exception: {ex.Message}");
                        }
                    },
                    () => {
                        try
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFields - Fundamental4 receiving");

                            using var dbNQ = new NQDBContext();
                            Fundamental4List = dbNQ.Fundamental4
                             .Where(f => !string.IsNullOrEmpty(f.URL) && domains.Any(d => f.URL.ToLower().Contains(d)))
                             .ToList();

                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFields - Fundamental4 received: {Fundamental4List.Count}");
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (Fundamental4) exception: {ex.Message}");
                        }
                    });

                Parallel.ForEach(domains, new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, (domain, state, index) =>
                {
                    try
                    {
                        ConsoleHelper.WriteLine(ConsoleColor.White, $"GetAllFields - Company processing - {domain}");

                        using var dbNQ = new NQDBContext();

                        var company = new CompanySummaryItem
                        {
                            domain = domain
                        };

                        // NQ ClearbitCompanyAPIs
                        try
                        {
                            var cl = clearbitCompanyAPIsList.FirstOrDefault(c => c.domain.ToLower() == domain.ToLower());
                            if (cl != null)
                            {
                                if (cl.metrics_annualRevenue.HasValue)
                                {
                                    company.annualrevenue = cl.metrics_annualRevenue.Value.ToString();
                                }
                                else if (!string.IsNullOrEmpty(cl.metrics_estimatedAnnualRevenue))
                                {
                                    company.annualrevenue = cl.metrics_estimatedAnnualRevenue;
                                }

                                if (!string.IsNullOrEmpty(cl.email)) company.company_email = cl.email;
                                if (!string.IsNullOrEmpty(cl.geo_city)) company.city = cl.geo_city;
                                if (!string.IsNullOrEmpty(cl.geo_country)) company.country = cl.geo_country;
                                if (!string.IsNullOrEmpty(cl.description)) company.description = cl.description;
                                if (!string.IsNullOrEmpty(cl.category_industryGroup)) company.industry = cl.category_industryGroup;
                                if (!string.IsNullOrEmpty(cl.name)) company.name = cl.name;
                                if (cl.employees.HasValue) company.numberofemployees = cl.employees.Value.ToString();
                                if (!string.IsNullOrEmpty(cl.phone)) company.phone = cl.phone;
                                if (!string.IsNullOrEmpty(cl.geo_stateCode)) company.state = cl.geo_stateCode;
                                company.address = string.Format("{0}{1}", string.IsNullOrEmpty(cl.geo_streetNumber) ? string.Empty : cl.geo_streetNumber + " ",
                                                                            string.IsNullOrEmpty(cl.geo_streetName) ? string.Empty : cl.geo_streetName);
                                if (!string.IsNullOrEmpty(cl.geo_subPremise)) company.address2 = cl.geo_subPremise;
                                if (!string.IsNullOrEmpty(cl.timeZone)) company.timezone = cl.timeZone;
                                if (cl.raised.HasValue) company.total_money_raised = cl.raised.Value.ToString();
                                if (!string.IsNullOrEmpty(cl.site_url)) company.website = cl.site_url;
                                if (cl.foundedYear.HasValue) company.founded_year = cl.foundedYear.Value.ToString();
                                if (!string.IsNullOrEmpty(cl.facebook_handle)) company.facebook_company_page = cl.facebook_handle;
                                if (cl.facebook_likes.HasValue) company.facebookfans = cl.facebook_likes.Value.ToString();
                                if (!string.IsNullOrEmpty(cl.googleplus_handle)) company.googleplus_page = cl.googleplus_handle;
                                if (!string.IsNullOrEmpty(cl.linkedin_handle)) company.linkedin_company_page = cl.linkedin_handle;
                                if (!string.IsNullOrEmpty(cl.twitter_site)) company.twitterbio = cl.twitter_site;
                                if (cl.twitter_followers.HasValue) company.twitterfollowers = cl.twitter_followers.Value.ToString();
                                if (!string.IsNullOrEmpty(cl.twitter_handle)) company.twitterhandle = cl.twitter_handle;
                            }
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (ClearbitCompanyAPIs) exception: {ex.Message}");
                        }

                        // NQ NDNews
                        try
                        {
                            company.working_with_competitor = NDNewsDomains.Any(n => n.Contains(domain)) ? "Yes" : "No";
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (NDNews) exception: {ex.Message}");
                        }

                        // NQ NDcrmMasters
                        try
                        {
                            var crm = NDcrmMasterList.FirstOrDefault(n => !string.IsNullOrEmpty(n.CompanyUrl) && n.CompanyUrl.ToLower() == domain.ToLower());
                            if (crm != null)
                            {
                                if (!string.IsNullOrEmpty(crm.PIDs)) company.competitor = crm.PIDs;
                                company.distributes_earnings = (crm.Earnings.HasValue && crm.Earnings.Value == true) ? "Yes" : "No";
                                company.multimedia_sender = (crm.Multimedia_Sender.HasValue && crm.Multimedia_Sender.Value == true) ? "Yes" : "No";
                                if (crm.StoryCount.HasValue) company.word_count_average_ = Math.Round(crm.StoryCount.Value, 0).ToString();
                                company.amazon_affiliate_links = (crm.AmazonLinks.HasValue && crm.AmazonLinks.Value == true) ? "Yes" : "No";
                                if (!string.IsNullOrEmpty(crm.AgencyUrl)) company.related_agency = crm.AgencyUrl;
                                if (!string.IsNullOrEmpty(crm.CompanyType) && crm.CompanyType.ToLower() == "agency") company.company_type = "PR Agency";
                                company.eav_high_current_year_ = crm.EAVHighCurrYear.HasValue ? Math.Round(crm.EAVHighCurrYear.Value, 0) : 0.0;
                                company.eav_high_prior_year_ = crm.EAVHighPrevYear.HasValue ? Math.Round(crm.EAVHighPrevYear.Value, 0) : 0.0;
                                company.eav_low_current_year_ = crm.EAVLowCurrYear.HasValue ? Math.Round(crm.EAVLowCurrYear.Value, 0) : 0.0;
                                company.eav_low_prior_year_ = crm.EAVLowPrevYear.HasValue ? Math.Round(crm.EAVLowPrevYear.Value, 0) : 0.0;
                                company.release_count_prior_year_ = crm.PrevYearReleases ?? 0;
                                company.release_count_current_year_ = crm.CurrYearReleases ?? 0;

                                company.static_media_count_current_year_ = crm.StaticMediaCount_Curr ?? 0;
                                company.static_media_count_previous_year_ = crm.StaticMediaCount_Prev ?? 0;
                                company.multimedia_count_current_year_ = crm.MultiMediaCount_Curr ?? 0;
                                company.multimedia_count_previous_year_ = crm.MultiMediaCount_Prev ?? 0;

                                company.static_media_sender = (crm.StaticMedia_Sender.HasValue && crm.StaticMedia_Sender.Value == true) ? "Yes" : "No";
                                company.ecv_prior_year_ = crm.ND_ECVPrevYear ?? 0;
                                company.ecv_current_year_ = crm.ND_ECVCurrYear ?? 0;
                                company.hubspot_owner_id = crm.hubspot_owner_id ?? 0;
                                company.prospect_level = crm.prospect_level ?? string.Empty;
                                company.public_company_dashboard_nq_ = crm.PublicCompanyDashboardURL ?? string.Empty;
                            }
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (NDcrmMasters) exception: {ex.Message}");
                        }

                        // Fundamental4
                        try
                        {
                            if (!company.is_public)
                            {
                                var f4 = dbNQ.Fundamental4.FirstOrDefault(f => !string.IsNullOrEmpty(f.URL) && f.URL.ToLower().Contains(domain.ToLower()));
                                if (f4 != null)
                                {
                                    if (string.IsNullOrEmpty(company.ticker_symbol) && !string.IsNullOrEmpty(f4.Symbol))
                                    {
                                        company.ticker_symbol = f4.Symbol;
                                        company.is_public = true;
                                    }
                                    if (string.IsNullOrEmpty(company.public_company_exchange) && !string.IsNullOrEmpty(f4.Exchange)) company.public_company_exchange = f4.Exchange;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (Fundamental4) exception: {ex}");
                        }

                        try
                        {
                            if (company.is_public && !string.IsNullOrEmpty(company.ticker_symbol) && !string.IsNullOrEmpty(company.public_company_exchange))
                            {
                                var ff = dbNQ.Fundamentals.Where(f => f.Symbol.ToLower() == company.ticker_symbol.ToLower() && f.Exchange.ToLower() == company.public_company_exchange.ToLower())
                                                        .OrderByDescending(f => f.JDate)
                                                        .Take(1)
                                                        .FirstOrDefault();

                                if (ff != null)
                                {
                                    company.market_cap = (ff.MktCap.HasValue) ? ff.MktCap.Value.ToString() : string.Empty;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB (Fundamentals) exception: {ex}");
                        }

                        if (!string.IsNullOrEmpty(company.description)) company.description = StringHelper.ClearString(company.description);
                        if (!string.IsNullOrEmpty(company.name)) company.name = StringHelper.ClearString(company.name);
                        if (!string.IsNullOrEmpty(company.phone)) company.phone = StringHelper.ClearString(company.phone);

                        lock (lockObject)
                        {
                            result.Add(company);
                        }

                        ConsoleHelper.WriteLine(ConsoleColor.White, $"GetAllFields - Company Precessed - {domain}: [ {index + 1} ] / [ {domains.Count} ]");
                    }
                    catch (Exception ex)
                    {
                        ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFields - Company Precess exception: {ex.Message}");
                    }
                }); // parallel
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllFieldsForDomainListForCompanyFromDB exception: {ex.Message}");
            }

            return result;
        }

        public static List<ClearbitPersonAPI> DB_PersonsList;
        public static object _lock_DB_PersonsList = new();

        public static void Get_DB_Persons_list()
        {
            try
            {
                lock (_lock_DB_PersonsList)
                {
                    if (DB_PersonsList == null || DB_PersonsList.Count == 0)
                    {
                        using var db = new NQDBContext();
                        DB_PersonsList = db.ClearbitPersonAPIs.ToList();
                    }
                }
            }
            catch(Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"Get_DB_Persons_list exception: {ex}");
            }
        }

        public static List<ParserNewswire> GetAllEmailRecordsFromDomains(IEnumerable<string> domains)
        {
            List<ParserNewswire> emailRecords = null;
            try
            {
                using var db = new NQDBContext();

                var domainsFilter = domains.Select(x => "(LOWER(p.Email) LIKE N'%@" + x + "')").Join(" OR ");

                var queryStr = $@"SELECT * FROM ParserNewswires AS p
                                  WHERE (NOT (p.Email LIKE N'%,%') AND NOT (p.Email LIKE N'%;%') AND NOT (p.Email LIKE N'% %') AND ({domainsFilter}))";

                // TODO: add to DB column 'email_domain' with COLLATE ci to speed up the query
                emailRecords = db.ParserNewswires.FromSqlRaw(queryStr)
                    .ToList()
                    .GroupBy(p => p.Email.ToLower())
                    .Select(g => g.FirstOrDefault())
                    .ToList();
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllEmailRecordsFromDomains exception: {ex}");
            }

            return emailRecords ?? new List<ParserNewswire>();
        }

        public static List<ContactSummaryItem> GetAllContactsFromDomains(List<ParserNewswire> records, bool checkByDebounce)
        {
            List<ContactSummaryItem> list = new();

            try
            {
                using var db = new NQDBContext();
                
                var recordGroups = records
                    .GroupBy(x => x.Email.ToLower());

                bool dbSaveRequired = false;
                foreach (var group in recordGroups)
                {
                    var email = group.Key;

                    int _checkCode = 0;
#if (DEBUG)
                    //_checkCode = 5; // test
#endif

                    if (_checkCode == 0 && checkByDebounce)
                    {
                        var _checkedRecord = group.FirstOrDefault(e => e.CheckByDebounce.HasValue);

                        if (_checkedRecord != null)
                        {
                            _checkCode = _checkedRecord.CheckByDebounce.Value;
                            foreach (var record in group)
                            {
                                if (record.CheckByDebounce != _checkCode)
                                {
                                    db.Attach(record);
                                    record.CheckByDebounce = _checkCode;
                                    dbSaveRequired = true;
                                }
                            }
                        }
                        else
                        {
                            DebounceItem debounceItem = DebounceHelper.GetDebounceItem(email);

                            ConsoleHelper.WriteLine(ConsoleColor.DarkYellow,
                                $"Email {email} checked by Debounce. Code: {debounceItem?.debounce?.code}. Balance: {debounceItem?.balance}");

                            if (debounceItem != null && debounceItem.debounce.code != 0)
                            {
                                _checkCode = debounceItem.debounce.code;
                                foreach (var record in group)
                                {
                                    if (record.CheckByDebounce != _checkCode)
                                    {
                                        db.Attach(record);
                                        record.CheckByDebounce = _checkCode;
                                        dbSaveRequired = true;
                                    }
                                }
                            }
                        }
                    }


                    if (!checkByDebounce ||
                        (_checkCode != 0 && (_checkCode == 5 || _checkCode == 4 || _checkCode == 8)))
                    {
                        List<ContactFromDB> listDB = new();
                        foreach (var record in group)
                        {
                            listDB.Add(new ContactFromDB()
                            {
                                Name = record.Name,
                                Email = record.Email,
                                CompanyName = record.CompanyName,
                                Title = record.Title,
                                NewsID = record.NewsID,
                                NewsReleaseDate = record.NewsReleaseDate,
                                Phone = record.Phone,
                            });
                        }

                        var contact = listDB.OrderByDescending(l => l.CompleteResult)
                                            .ThenByDescending(l => l.NewsReleaseDate)
                                            .FirstOrDefault();

                        if (contact != null)
                        {
                            ContactSummaryItem item = new();

                            string lname = string.Empty;
                            string fname = string.Empty;
                            if (!string.IsNullOrEmpty(contact.Name))
                            {
                                if (contact.Name.Contains(" "))
                                {
                                    fname = contact.Name.Split(new string[] { " " }, StringSplitOptions.None).First();
                                    lname = contact.Name.Split(new string[] { fname }, StringSplitOptions.None).Last().Trim();
                                }
                                else
                                {
                                    fname = FirstLetterToUpper(contact.Name);
                                }
                            }

                            item.company = contact.CompanyName;
                            item.email = contact.Email;
                            item.firstname = FirstLetterToUpper(fname);
                            item.lastname = FirstLetterToUpper(lname);
                            item.jobtitle = contact.Title;
                            item.nq_contact_date = contact.NewsReleaseDate;
                            item.nq_source = contact.NewsID;
                            item.phone = contact.Phone;

                            if (!string.IsNullOrEmpty(item.email) && !string.IsNullOrEmpty(item.phone) && email.Contains(item.phone))
                            {
                                item.email = email.Split(new string[] { item.phone }, StringSplitOptions.None).Last();
                            }

                            if (!list.Any(a => a.email.ToLower() == item.email.ToLower()))
                            {
                                if (DB_PersonsList == null || DB_PersonsList.Count == 0)
                                {
                                    Get_DB_Persons_list();
                                }

                                if (DB_PersonsList != null)
                                {
                                    var _dbpers = DB_PersonsList.FirstOrDefault(p => p.email.ToLower() == item.email.ToLower());
                                    if (_dbpers != null)
                                    {
                                        item.full_name = _dbpers.name_fullName;
                                        item.location = _dbpers.location;
                                        item.city = _dbpers.geo_city;
                                        item.state = _dbpers.geo_state;
                                        item.country = _dbpers.geo_country;
                                        item.linkedin = _dbpers.linkedin_handle;
                                        item.employment_role = _dbpers.employment_role;
                                        item.employment_subrole = _dbpers.employment_subRole;
                                        item.seniority = _dbpers.employment_seniority;

                                        if (!string.IsNullOrEmpty(_dbpers.bio))
                                        {
                                            item.bio = _dbpers.bio.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Replace("\"", "'");
                                        }

                                        if (string.IsNullOrEmpty(item.company)) item.company = _dbpers.employment_name;
                                        if (string.IsNullOrEmpty(item.firstname)) item.firstname = _dbpers.name_givenName;
                                        if (string.IsNullOrEmpty(item.lastname)) item.lastname = _dbpers.name_familyName;
                                        if (string.IsNullOrEmpty(item.jobtitle)) item.jobtitle = _dbpers.employment_title;
                                        if (string.IsNullOrEmpty(item.phone)) item.phone = _dbpers.Phone;
                                    }
                                }

                                list.Add(item);
                            }
                        }
                    }
                }

                if (dbSaveRequired)
                {
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Red, $"GetAllContactsFromDomains exception: {ex}");
            }

            return list;
        }

        public static string FirstLetterToUpper(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = str.ToLower();
                return char.ToUpper(str[0]) + str[1..];
            }
            return string.Empty;
        }

        public static async Task<List<CompanyProcessContainer>> GetCompanyListAsync(string domain = null)
        {
            var dbCompanyList = await GetCompaniesForUpdateAsync(domain);
            var result = dbCompanyList.Select(x => new CompanyProcessContainer { DBRecord = x }).ToList();

            ConsoleHelper.WriteLine(ConsoleColor.Red, $"CompanyItemDB : downloaded. Count: {result.Count}");
#if (!DEBUG)
            if (result.Count == 0)
                MailHelper.SendEmails("Main", "DBFunc.GetCompaniesForUpdate", "CompanyList is Empty");
#endif

            return result;
        }
    }
}
