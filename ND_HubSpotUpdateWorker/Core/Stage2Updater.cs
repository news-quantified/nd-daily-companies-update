﻿using ND_HubSpotUpdateWorker.Dto;
using ND_HubSpotUpdateWorker.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Core
{
    /// <summary>
    /// create new companies
    /// </summary>
    public static class Stage2Updater
    {
        public static void Run(List<CompanyProcessContainer> companyList)
        {
            var companyListPrioritized = companyList.Where(x => !x.DBRecord.ND_HubSpotPrioritized.HasValue ||
                (x.DBRecord.ND_HubSpotPrioritized.HasValue && x.DBRecord.LastRelease.HasValue && x.DBRecord.LastRelease.Value > x.DBRecord.ND_HubSpotPrioritized.Value) ||
                (x.DBRecord.ND_HubSpotPrioritized.HasValue && x.DBRecord.LastUpdate.HasValue && x.DBRecord.LastUpdate.Value > x.DBRecord.ND_HubSpotPrioritized.Value));

            var newCompaniesList = companyListPrioritized
                .Where(x => x.HSRecords.Count == 0 && x.Error == null) // not found in HS
                .ToList();

            ConsoleHelper.WriteLine(ConsoleColor.White, $" +++ Stage 2 started. Records count: {newCompaniesList.Count}");

            Interlocked.Exchange(ref Globals.COUNT_STAGE2_TOTAL, newCompaniesList.Count);

            Parallel.ForEach(newCompaniesList, new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, company =>
            {
                Interlocked.Increment(ref Globals.COUNT_STAGE2_PROCESSED);
                CreateCompany(company);
            });

            ConsoleHelper.WriteLine(ConsoleColor.White, $" +++ Stage 2 finished. Records processed: {companyListPrioritized.Where(x => x.Created).Count()}");
        }

        static void CreateCompany(CompanyProcessContainer company)
        {
            CompanySummaryItem CompanyItemDB = null;
            CompanySummaryItem createdCompanyItem = null;

            try
            {
                company.logId = DBFunc.GetIdForLog(company.DBRecord.CompanyUrl); ;

                if (company.logId != 0 && company.Error == null)
                {
                    if (!HubSpotHelper.IsDomainValid(company.DBRecord.CompanyUrl))
                    {
                        company.Error = $"Invalid domain: {company.DBRecord.CompanyUrl}";
                        DBFunc.CompanyNotAddedToHS(company.logId, "Create", "", company.Error);
                        Interlocked.Increment(ref Globals.COUNT_COMPANY_ADD_ERROR);
                        return;
                    }

                    List<CompanySummaryItem> companyItemHSList = null;

                    Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism },
                        () => {
                            try
                            {
                                companyItemHSList = HubSpotHelper.GetCompanyFromHubSpotByDomain(company.DBRecord.CompanyUrl.ToLower());
                            }
                            catch (Exception ex)
                            {
                                company.Error = ex.ToString();
                            }
                        },
                        () => {
                            try
                            {
                                CompanyItemDB = DBFunc.GetAllFieldsForCompanyFromDB(company.DBRecord.CompanyUrl);
                            }
                            catch (Exception ex)
                            {
                                company.Error = ex.ToString();
                            }
                            
                        });

                    if (CompanyItemDB != null && string.IsNullOrEmpty(company.Error))
                    {
                        if (companyItemHSList == null || companyItemHSList.Count == 0)
                        {
                            try
                            {
                                HubSpotHelper.RecheckCompanyBeforeUpdate(CompanyItemDB);
                                CompanyItemDB.json = HubSpotHelper.CompanyToJson(CompanyItemDB, true);

                                company.HSRecords.Add(CompanyItemDB);
                                company.Created = true;

                                createdCompanyItem = HubSpotHelper.AddCompanyToHubSpot(CompanyItemDB, company.logId);

                                if (createdCompanyItem != null)
                                {
                                    createdCompanyItem.json = HubSpotHelper.CompanyToJson(CompanyItemDB, true);

                                    company.HSRecords.Clear();
                                    company.HSRecords.Add(createdCompanyItem);
                                }

                                DBFunc.UpdatePrioritizedFieldsInNDcrmMasters(company.DBRecord);
                                ConsoleHelper.WriteLine(ConsoleColor.Green, string.Format("Domain : {0} added - OK", company.DBRecord.CompanyUrl));
                                Interlocked.Increment(ref Globals.COUNT_COMPANY_ADD);
                            }
                            catch (Exception ex)
                            {
                                company.Error = string.Format("Domain : {0} Add - Exeption - {1}", company.DBRecord.CompanyUrl, ex.Message);
                                Interlocked.Increment(ref Globals.COUNT_COMPANY_ADD_ERROR);
                                ConsoleHelper.WriteLine(ConsoleColor.Cyan, company.Error);
                            }
                        }
                        else
                        {
                            foreach (var comp in companyItemHSList)
                            {
                                comp.json = HubSpotHelper.CompanyToJson(CompanyItemDB, true);
                            }
                            company.HSRecords.AddRange(companyItemHSList);

                            ConsoleHelper.WriteLine(ConsoleColor.Red, $"CreateCompany error: company already exist. {company.DBRecord.CompanyUrl}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Cyan, string.Format("Domain : {0} GENERAL ESCEPTION : {1}", company.DBRecord.CompanyUrl, ex.Message));
            }
        }
    }
}
