﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ND_HubSpotUpdateWorker.Core
{
    public static class Globals
    {
        #region params

        private static List<string> _applicationArguments = new();

        public static List<string> ApplicationArguments {
            get {
                return _applicationArguments;
            }
            set {
                _applicationArguments = value;

                // params
                // -s1 - stage 1: update hi-priority data 
                // -s2 - stage 2: create new companies
                // -s3 - stage 3: add contacts, update low-priority data
                // -t:N - MaxDegreeOfParallelism. Default = 12
                // -domain:<domainname> - single domain to process
                bool stage1Param = _applicationArguments.Contains("-s1");
                bool stage2Param = _applicationArguments.Contains("-s2");
                bool stage3Param = _applicationArguments.Contains("-s3");
                bool noStageParams = !stage1Param && !stage2Param && !stage3Param;

                string maxThreadsParamArg = _applicationArguments.FirstOrDefault(x => x.StartsWith("-t:"));
                if (maxThreadsParamArg?.Length > 3)
                {
                    _ = int.TryParse(maxThreadsParamArg?[3..], out MaxDegreeOfParallelism);
                }

                string singleDomainArg = _applicationArguments.FirstOrDefault(x => x.StartsWith("-domain:"));
                if (singleDomainArg?.Length > 8)
                {
                    SingleDomainParam = singleDomainArg?[8..];
                }
            }
        }

        public static string SingleDomainParam;

        public static bool Stage1Param => ApplicationArguments.Contains("-s1");
        public static bool Stage2Param => ApplicationArguments.Contains("-s2");
        public static bool Stage3Param => ApplicationArguments.Contains("-s3");
        public static bool NoStageParams => !Stage1Param && !Stage2Param && !Stage3Param;

        #endregion params

        public static int MaxDegreeOfParallelism = 12;

        public static int COUNT_STAGE1_PROCESSED = 0;
        public static int COUNT_STAGE1_TOTAL = 0;

        public static int COUNT_STAGE2_PROCESSED = 0;
        public static int COUNT_STAGE2_TOTAL = 0;

        public static int COUNT_STAGE3_PROCESSED = 0;
        public static int COUNT_STAGE3_TOTAL = 0;

        public static int COUNT_COMPANY_ADD = 0;
        public static int COUNT_COMPANY_ADD_ERROR = 0;

        public static int COUNT_COMPANY_UPDATE_HI = 0;
        public static int COUNT_COMPANY_UPDATE_LOW = 0;

        public static int COUNT_COMPANY_UPDATE_ERROR_HI = 0;
        public static int COUNT_COMPANY_UPDATE_ERROR_LOW = 0;

        public static int COUNT_CONTACT_ADD = 0;
        public static int COUNT_CONTACT_ADD_ERROR = 0;

        public static int COUNT_CONTACT_UPDATE = 0;
        public static int COUNT_CONTACT_UPDATE_ERROR = 0;

        public static readonly SemaphoreSlim DbSemaphore = new(1, 12);

        public static List<string> EmailsForReport = new()
        {
            "sergei.arienchuk@newsquantified.com",
            "serhii@newsquantified.com"
        };
    }
}
