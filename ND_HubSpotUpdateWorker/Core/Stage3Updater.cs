﻿using ND_HubSpotUpdateWorker.Dto;
using ND_HubSpotUpdateWorker.Extensions;
using ND_HubSpotUpdateWorker.Helper;
using ND_HubSpotUpdateWorker.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Core
{
    /// <summary>
    /// add new contacts and update low-priority data of existed companies
    /// </summary>
    public static class Stage3Updater
    {
        public static void Run(List<CompanyProcessContainer> companyList)
        {
            companyList = companyList.Where(x => x.Error == null).ToList();

            ConsoleHelper.WriteLine(ConsoleColor.White, $" +++ Stage 3 started. Records count: {companyList.Count}");

            Interlocked.Exchange(ref Globals.COUNT_STAGE3_TOTAL, companyList.Count);

            Parallel.ForEach(companyList.ChunkBy(100), new ParallelOptions { MaxDegreeOfParallelism = 1 }, chunk =>
            {
                Interlocked.Increment(ref Globals.COUNT_STAGE3_PROCESSED);
                BatchProcess(chunk.ToList());
            });

            ConsoleHelper.WriteLine(ConsoleColor.White, $" +++ Stage 3 finished. Records processed: {companyList.Where(x => x.UpdatedLow).Count()}");
        }

        static void BatchProcess(List<CompanyProcessContainer> companyProcessList)
        {
            try
            {
                CollectCompanyData(companyProcessList);

                var duplicatesList = companyProcessList.Where(x => x.HSRecords?.Count > 0 && x.Created).ToList();
                if (duplicatesList.Count > 0)
                {
                    var message = $"Duplicates found: " + duplicatesList.Select(x => x.DBRecord.CompanyUrl).Join();
                    ConsoleHelper.WriteLine(ConsoleColor.Magenta, message);
                    MailHelper.SendEmails("Stage3", "Duplicate check", message);
                }

                companyProcessList = companyProcessList.Where(x => x.HSRecords.Count > 0 && x.Error == null).ToList();

                UpdateCompanies(companyProcessList);

                UpdateOrCreateContacts(companyProcessList);

            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine(ConsoleColor.Cyan, $"STAGE3 BatchProcess - GENERAL ESCEPTION : {ex.Message}");
            }
        }



        #region private

        private static void CollectCompanyData(List<CompanyProcessContainer> companyProcessList)
        {
            Parallel.ForEach(companyProcessList, new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, company =>
            {
                if (!HubSpotHelper.IsDomainValid(company.DBRecord.CompanyUrl))
                {
                    company.Error = $"Invalid domain: {company.DBRecord.CompanyUrl}";

                    if (company.logId > 0)
                        DBFunc.CompanyNotAddedToHS(company.logId, "", company.HSRecords.Select(x => x.json).Join(), company.Error);
                    else
                        DBFunc.GetIdForLog(company.DBRecord.CompanyUrl, company.Error);

                    Interlocked.Increment(ref Globals.COUNT_COMPANY_UPDATE_ERROR_LOW);
                }
                else
                {
                    //re-read from the hubspot to reduce the risk of overwriting irrelevant data. Therefore, line is commented out
                    // if (company.HSRecords.Count == 0)
                    {
                        try
                        {
                            company.HSRecords = HubSpotHelper.GetCompanyFromHubSpotByDomain(company.DBRecord.CompanyUrl.ToLower());
                        }
                        catch (Exception ex)
                        {
                            company.Error = ex.ToString();
                        }
                    }

                    if (company.HSRecords.Count > 1)
                    {
                        ConsoleHelper.WriteLine(ConsoleColor.DarkCyan,
                            $"Found {company.HSRecords.Count} HS companies by one domain: " +
                            $"{company.HSRecords.Select(x => x.domain).Join()}");
                    }

                    if (company.HSRecords?.Count > 0 && company.Error == null)
                    {
                        try
                        {
                            var CompanyItemDB = DBFunc.GetAllFieldsForCompanyFromDB(company.DBRecord.CompanyUrl);

                            MapCompanyDBToHS_LowPriority(CompanyItemDB, company);
                            company.HSRecords.ForEach(x =>
                            {
                                HubSpotHelper.RecheckCompanyBeforeUpdate(x);
                                x.json = HubSpotHelper.CompanyToJson(x, true);
                            });
                        }
                        catch (Exception ex)
                        {
                            company.Error = ex.ToString();
                        }
                    }
                }
            });
        }

        private static void UpdateCompanies(List<CompanyProcessContainer> companyProcessList)
        {
            DBFunc.GetIdsForLog(companyProcessList, "Log added");

            try
            {
                HubSpotHelper.UpdateGroupOfCompaniesToHubSpot(companyProcessList);
                DBFunc.CompanyListAddedToHS(companyProcessList, "Update");

                companyProcessList.ForEach(x => x.UpdatedLow = true);
                Interlocked.Add(ref Globals.COUNT_COMPANY_UPDATE_LOW, companyProcessList.Count);

                ConsoleHelper.WriteLine(ConsoleColor.Green, $"Batch ({companyProcessList.Count} companies) of Domains updated - OK");
                DBFunc.UpdateFieldsInNDcrmMasters(companyProcessList.Select(x => x.DBRecord));
            }
            catch //(Exception ex1)
            {
                var companyProcessListChunked = companyProcessList
                    .ChunkBy(Math.Max(companyProcessList.Count / 10, 1));

                ConsoleHelper.WriteLine(ConsoleColor.Magenta, $" - Chunks of Batch processing");
                Parallel.ForEach(companyProcessListChunked, new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, chunk =>
                {
                    try
                    {
                        HubSpotHelper.UpdateGroupOfCompaniesToHubSpot(chunk.ToList());
                        DBFunc.CompanyListAddedToHS(companyProcessList, "Update");

                        chunk.ToList().ForEach(x => x.UpdatedLow = true);
                        Interlocked.Add(ref Globals.COUNT_COMPANY_UPDATE_LOW, chunk.Count());

                        ConsoleHelper.WriteLine(ConsoleColor.Green, $"- Chunk ({chunk.Count()}) of Batch updated - OK");
                        DBFunc.UpdateFieldsInNDcrmMasters(chunk.Select(x => x.DBRecord));
                    }
                    catch //(Exception ex2)
                    {
                        foreach (var company in chunk)
                        {
                            try
                            {
                                var res = HubSpotHelper.UpdateCompanyToHubSpot(company, true);
                                DBFunc.CompanyAddedToHS(company.logId, "Update",
                                    company.HSRecords.Select(x => x.json).Join(),
                                    res.Join());

                                DBFunc.UpdateFieldsInNDcrmMasters(company.DBRecord);

                                company.UpdatedLow = true;
                                Interlocked.Increment(ref Globals.COUNT_COMPANY_UPDATE_LOW);
                                ConsoleHelper.WriteLine(ConsoleColor.Green, string.Format("Domain : {0} updated - OK", company.DBRecord.CompanyUrl));
                            }
                            catch (Exception ex3)
                            {
                                DBFunc.CompanyNotAddedToHS(company.logId, "Update", company.HSRecords.Select(x => x.json).Join(), ex3.Message);
                                Interlocked.Increment(ref Globals.COUNT_COMPANY_UPDATE_ERROR_LOW);
                                ConsoleHelper.WriteLine(ConsoleColor.Cyan, string.Format("Domain : {0} Update - Exeption - {1}", company.DBRecord.CompanyUrl, ex3.Message));
                            }
                        }
                    }
                });
            }
        }
        
        private static void UpdateOrCreateContacts(List<CompanyProcessContainer> companyProcessList)
        {
            var domains = companyProcessList.Select(x => x.DBRecord.CompanyUrl.ToLower());
            ConsoleHelper.WriteLine(ConsoleColor.DarkYellow, $"UpdateOrCreateContacts T:{Thread.CurrentThread.ManagedThreadId} Count:{domains.Count()} => Started");

            List<ParserNewswire> emailRecords = DBFunc.GetAllEmailRecordsFromDomains(domains);
            List<string> dbEmails = emailRecords.Select(x => x.Email.ToLower()).ToList();

            List<ContactSummaryItem> hsContacts = HubSpotHelper.GetContactsByEmails(dbEmails);

            // Update existing contacts
            List<ParserNewswire> existingContactsDB = emailRecords.Where(x => hsContacts.Any(hs => hs.email.ToLower() == x.Email.ToLower())).ToList();
            List<ContactSummaryItem> existingContactsModels = DBFunc.GetAllContactsFromDomains(existingContactsDB, checkByDebounce: false);

            List<ContactSummaryItem> hsContactsToUpdate = hsContacts.Where(x => existingContactsModels.Any(hs => hs.email.ToLower() == x.email.ToLower())).ToList();
            var updatedContacts = MapContactsDbToHS(existingContactsModels, hsContactsToUpdate);

            Parallel.ForEach(updatedContacts.ChunkBy(10), new ParallelOptions { MaxDegreeOfParallelism = Globals.MaxDegreeOfParallelism }, chunk =>
            {
                UpdateBatchOfContacts(chunk.ToList());
            });


            // Create new contact
            List<ParserNewswire> newContactsDB = emailRecords.Where(x => !hsContacts.Any(hs => hs.email.ToLower() == x.Email.ToLower())).ToList();
            List<ContactSummaryItem> newContactsModels = DBFunc.GetAllContactsFromDomains(newContactsDB, checkByDebounce: true);

            foreach (var contact in newContactsModels)
            {
                var company = companyProcessList
                    .FirstOrDefault(x => contact.email.ToLower().Contains("@" + x.DBRecord.CompanyUrl.ToLower()));

                if (company?.logId > 0 && company.HSRecords?.Count > 0 && company.Error == null)
                {
                    try
                    {
                        HubSpotHelper.AddContactToHubSpot(contact);
                        DBFunc.CompanyAddedContact(company.logId, contact.email);

                        company.AddedContactEmails.Add(contact.email);
                        Interlocked.Increment(ref Globals.COUNT_CONTACT_ADD);
                        ConsoleHelper.WriteLine(ConsoleColor.Yellow, string.Format("Email : {0} added - OK", contact.email));
                    }
                    catch (Exception e)
                    {
                        company.NotAddedContactEmails.Add(contact.email);
                        Interlocked.Increment(ref Globals.COUNT_CONTACT_ADD_ERROR);

                        if (e.InnerException?.Message == "409")
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.DarkCyan,
                                string.Format("Email : {0} Add - Exeption - {1}", contact.email, "Contact already exists"));
                        }
                        else
                        {
                            ConsoleHelper.WriteLine(ConsoleColor.Cyan,
                                string.Format("Email : {0} Add - Exeption - {1}", contact.email, e.InnerException?.Message ?? e.Message));
                        }
                    }
                }
            }

            ConsoleHelper.WriteLine(ConsoleColor.Yellow, $"UpdateOrCreateContacts T:{Thread.CurrentThread.ManagedThreadId} Count:{domains.Count()} => Finished");
        }

        private static void UpdateBatchOfContacts(List<ContactSummaryItem> hsContactsToUpdate)
        {
            try
            {
                HubSpotHelper.UpdateBatchOfContacts(hsContactsToUpdate);
                Interlocked.Exchange(ref Globals.COUNT_CONTACT_UPDATE, hsContactsToUpdate.Count);
                ConsoleHelper.WriteLine(ConsoleColor.DarkGreen, $"Contacts updated {hsContactsToUpdate.Count} - OK");// + hsContactsToUpdate.Select(x => x.email).Join()) ;
            }
            catch //(Exception e)
            {
                foreach (var contact in hsContactsToUpdate)
                {
                    try
                    {
                        HubSpotHelper.UpdateBatchOfContacts(new List<ContactSummaryItem> { contact });
                        Interlocked.Increment(ref Globals.COUNT_CONTACT_UPDATE);
                        //ConsoleHelper.WriteLine(ConsoleColor.DarkGreen, "Contact updated - OK: " + contact.email);
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Increment(ref Globals.COUNT_CONTACT_UPDATE_ERROR);
                        ConsoleHelper.WriteLine(ConsoleColor.Cyan, $"Contact {contact.email} Update - Exeption - {ex.Message}");
                    }
                }
            }
        }

        private static void MapCompanyDBToHS_LowPriority(CompanySummaryItem CompanyItemDB, CompanyProcessContainer companyProcess)
        {
            if (CompanyItemDB == null)
                return;

            foreach (var CompanyItemHS in companyProcess.HSRecords)
            {
                CompanyItemHS.HistoricalClone = CompanyItemHS.Clone;

                // low-priority data
                if (string.IsNullOrEmpty(CompanyItemHS.address) && !string.IsNullOrEmpty(CompanyItemDB.address)) CompanyItemHS.address = CompanyItemDB.address;
                if (string.IsNullOrEmpty(CompanyItemHS.address2) && !string.IsNullOrEmpty(CompanyItemDB.address2)) CompanyItemHS.address2 = CompanyItemDB.address2;
                if (string.IsNullOrEmpty(CompanyItemHS.annualrevenue) && !string.IsNullOrEmpty(CompanyItemDB.annualrevenue)) CompanyItemHS.annualrevenue = CompanyItemDB.annualrevenue;
                if (string.IsNullOrEmpty(CompanyItemHS.city) && !string.IsNullOrEmpty(CompanyItemDB.city)) CompanyItemHS.city = CompanyItemDB.city;
                if (string.IsNullOrEmpty(CompanyItemHS.company_email) && !string.IsNullOrEmpty(CompanyItemDB.company_email)) CompanyItemHS.company_email = CompanyItemDB.company_email;
                if (string.IsNullOrEmpty(CompanyItemHS.country) && !string.IsNullOrEmpty(CompanyItemDB.country)) CompanyItemHS.country = CompanyItemDB.country;
                if (string.IsNullOrEmpty(CompanyItemHS.description) && !string.IsNullOrEmpty(CompanyItemDB.description)) CompanyItemHS.description = CompanyItemDB.description;
                if (string.IsNullOrEmpty(CompanyItemHS.facebookfans) && !string.IsNullOrEmpty(CompanyItemDB.facebookfans)) CompanyItemHS.facebookfans = CompanyItemDB.facebookfans;
                if (string.IsNullOrEmpty(CompanyItemHS.facebook_company_page) && !string.IsNullOrEmpty(CompanyItemDB.facebook_company_page)) CompanyItemHS.facebook_company_page = CompanyItemDB.facebook_company_page;
                if (string.IsNullOrEmpty(CompanyItemHS.founded_year) && !string.IsNullOrEmpty(CompanyItemDB.founded_year)) CompanyItemHS.founded_year = CompanyItemDB.founded_year;
                if (string.IsNullOrEmpty(CompanyItemHS.googleplus_page) && !string.IsNullOrEmpty(CompanyItemDB.googleplus_page)) CompanyItemHS.googleplus_page = CompanyItemDB.googleplus_page;
                if (string.IsNullOrEmpty(CompanyItemHS.industry) && !string.IsNullOrEmpty(CompanyItemDB.industry)) CompanyItemHS.industry = CompanyItemDB.industry;
                if (string.IsNullOrEmpty(CompanyItemHS.investor_relations_site) && !string.IsNullOrEmpty(CompanyItemDB.investor_relations_site)) CompanyItemHS.investor_relations_site = CompanyItemDB.investor_relations_site;
                if (!CompanyItemHS.is_public) CompanyItemHS.is_public = CompanyItemDB.is_public;
                if (string.IsNullOrEmpty(CompanyItemHS.linkedin_company_page) && !string.IsNullOrEmpty(CompanyItemDB.linkedin_company_page)) CompanyItemHS.linkedin_company_page = CompanyItemDB.linkedin_company_page;
                CompanyItemHS.market_cap = CompanyItemDB.market_cap;
                if (string.IsNullOrEmpty(CompanyItemHS.name) && !string.IsNullOrEmpty(CompanyItemDB.name)) CompanyItemHS.name = CompanyItemDB.name;
                if (string.IsNullOrEmpty(CompanyItemHS.newsroom_url) && !string.IsNullOrEmpty(CompanyItemDB.newsroom_url)) CompanyItemHS.newsroom_url = CompanyItemDB.newsroom_url;
                if (string.IsNullOrEmpty(CompanyItemHS.numberofemployees) && !string.IsNullOrEmpty(CompanyItemDB.numberofemployees)) CompanyItemHS.numberofemployees = CompanyItemDB.numberofemployees;
                if (string.IsNullOrEmpty(CompanyItemHS.phone) && !string.IsNullOrEmpty(CompanyItemDB.phone)) CompanyItemHS.phone = CompanyItemDB.phone;
                if (string.IsNullOrEmpty(CompanyItemHS.public_company_exchange) && !string.IsNullOrEmpty(CompanyItemDB.public_company_exchange)) CompanyItemHS.public_company_exchange = CompanyItemDB.public_company_exchange;
                if (string.IsNullOrEmpty(CompanyItemHS.related_agency) && !string.IsNullOrEmpty(CompanyItemDB.related_agency)) CompanyItemHS.related_agency = CompanyItemDB.related_agency;
                if (string.IsNullOrEmpty(CompanyItemHS.state) && !string.IsNullOrEmpty(CompanyItemDB.state)) CompanyItemHS.state = CompanyItemDB.state;
                if (string.IsNullOrEmpty(CompanyItemHS.ticker_symbol) && !string.IsNullOrEmpty(CompanyItemDB.ticker_symbol)) CompanyItemHS.ticker_symbol = CompanyItemDB.ticker_symbol;
                if (string.IsNullOrEmpty(CompanyItemHS.timezone) && !string.IsNullOrEmpty(CompanyItemDB.timezone)) CompanyItemHS.timezone = CompanyItemDB.timezone;
                CompanyItemHS.total_money_raised = CompanyItemDB.total_money_raised;
                if (string.IsNullOrEmpty(CompanyItemHS.twitterbio) && !string.IsNullOrEmpty(CompanyItemDB.twitterbio)) CompanyItemHS.twitterbio = CompanyItemDB.twitterbio;
                CompanyItemHS.twitterfollowers = CompanyItemDB.twitterfollowers;
                if (string.IsNullOrEmpty(CompanyItemHS.twitterhandle) && !string.IsNullOrEmpty(CompanyItemDB.twitterhandle)) CompanyItemHS.twitterhandle = CompanyItemDB.twitterhandle;
                if (string.IsNullOrEmpty(CompanyItemHS.website) && !string.IsNullOrEmpty(CompanyItemDB.website)) CompanyItemHS.website = CompanyItemDB.website;
                CompanyItemHS.working_with_competitor = CompanyItemDB.working_with_competitor;
                if (string.IsNullOrEmpty(CompanyItemHS.public_company_dashboard_nq_) && !string.IsNullOrEmpty(CompanyItemDB.public_company_dashboard_nq_)) CompanyItemHS.public_company_dashboard_nq_ = CompanyItemDB.public_company_dashboard_nq_;

                // hi-priority data. update it again, why not
                CompanyItemHS.competitor = CompanyItemDB.competitor;
                CompanyItemHS.current_year_billing = CompanyItemDB.current_year_billing;
                CompanyItemHS.distributes_earnings = CompanyItemDB.distributes_earnings;
                CompanyItemHS.eav_high_current_year_ = CompanyItemDB.eav_high_current_year_;
                CompanyItemHS.eav_high_prior_year_ = CompanyItemDB.eav_high_prior_year_;
                CompanyItemHS.eav_low_current_year_ = CompanyItemDB.eav_low_current_year_;
                CompanyItemHS.eav_low_prior_year_ = CompanyItemDB.eav_low_prior_year_;
                CompanyItemHS.multimedia_sender = CompanyItemDB.multimedia_sender;
                CompanyItemHS.release_count_current_year_ = CompanyItemDB.release_count_current_year_;
                CompanyItemHS.release_count_prior_year_ = CompanyItemDB.release_count_prior_year_;
                CompanyItemHS.static_media_sender = CompanyItemDB.static_media_sender;
                CompanyItemHS.word_count = CompanyItemDB.word_count;
                CompanyItemHS.word_count_average_ = CompanyItemDB.word_count_average_;
                CompanyItemHS.static_media_count_current_year_ = CompanyItemDB.static_media_count_current_year_;
                CompanyItemHS.static_media_count_previous_year_ = CompanyItemDB.static_media_count_previous_year_;
                CompanyItemHS.multimedia_count_current_year_ = CompanyItemDB.multimedia_count_current_year_;
                CompanyItemHS.multimedia_count_previous_year_ = CompanyItemDB.multimedia_count_previous_year_;
                CompanyItemHS.ecv_current_year_ = CompanyItemDB.ecv_current_year_;
                CompanyItemHS.ecv_prior_year_ = CompanyItemDB.ecv_prior_year_;
                if (CompanyItemHS.hubspot_owner_id == 0) CompanyItemHS.hubspot_owner_id = CompanyItemDB.hubspot_owner_id;
                CompanyItemHS.prospect_level = CompanyItemDB.prospect_level;
                CompanyItemHS.public_company_dashboard_nq_ = CompanyItemDB.public_company_dashboard_nq_;
            }
        }

        public static string MapProp(string src, ref bool mapped)
        {
            if (!string.IsNullOrEmpty(src))
            {
                mapped = true;
                return src;
            }

            return null;
        }

        private static List<ContactSummaryItem> MapContactsDbToHS(List<ContactSummaryItem> srcList, List<ContactSummaryItem> destList)
        {
            List<ContactSummaryItem> result = new();

            foreach (var dest in destList)
            {
                var src = srcList.FirstOrDefault(x => x.email.ToLower() == dest.email.ToLower());
                if (src == null)
                    continue;

                bool mapped = false;

                dest.bio                ??= MapProp(src.bio, ref mapped);
                dest.city               ??= MapProp(src.city, ref mapped);
                dest.company            ??= MapProp(src.company, ref mapped);
                dest.contact_id         ??= MapProp(src.contact_id, ref mapped);
                dest.country            ??= MapProp(src.country, ref mapped);
                dest.employment_role    ??= MapProp(src.employment_role, ref mapped);
                dest.employment_subrole ??= MapProp(src.employment_subrole, ref mapped);
                dest.firstname          ??= MapProp(src.firstname, ref mapped);
                dest.full_name          ??= MapProp(src.full_name, ref mapped);
                dest.jobtitle           ??= MapProp(src.jobtitle, ref mapped);
                dest.lastname           ??= MapProp(src.lastname, ref mapped);
                dest.linkedin           ??= MapProp(src.linkedin, ref mapped);
                dest.location           ??= MapProp(src.location, ref mapped);
                dest.nq_source          ??= MapProp(src.nq_source, ref mapped);
                dest.phone              ??= MapProp(src.phone, ref mapped);
                dest.seniority          ??= MapProp(src.seniority, ref mapped);
                dest.state              ??= MapProp(src.state, ref mapped);

                if (mapped)
                {
                    if (dest.nq_contact_date == null && src.nq_contact_date != null)
                    {
                        dest.nq_contact_date = src.nq_contact_date;
                    }

                    result.Add(dest);
                }
            }

            return result;
        }
        #endregion private
    }
}
