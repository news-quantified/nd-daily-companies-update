﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND_HubSpotUpdateWorker.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Update_LowAttribute : Attribute
    {

    }

    [AttributeUsage(AttributeTargets.Property)]
    public class Update_HiAttribute : Attribute
    {

    }

    [AttributeUsage(AttributeTargets.Property)]
    public class Update_IfNotEmptyAttribute : Attribute
    {

    }

    [AttributeUsage(AttributeTargets.Property)]
    public class NotUpdateAttribute : Attribute
    {

    }
}
